import os
import numpy as np
import scipy as sp
from numpy.fft import fft,ifft
from scipy.fftpack import idct, dct
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from scipy import linalg
from scipy.fftpack import idct, dct

def DchebDCT(v, m):
	n=len(v)
	fac = 1/np.sqrt(2*(n-1))
	U = dct(v, type=1)*fac #type 1 does not support normalization yet
	
	for i in range(m): # mth derivative
		U1 = np.copy(U)
		U[-1] = 0
		U[-2] = 0
		for i in range(len(U)-2, 0, -1):
			U[i-1] = 2*i*U1[i] + U[i+1]
	w = idct(U, type=1)*fac
	return w

def dr(Vr):
	dr = np.empty_like(Vr)
	for i in range(M2):
		rr = np.hstack( (Vr[i::M], Vr[i+M2::M][::-1] ))
		back = 1./CellR*DchebDCT(rr,1)
		dr[i::M] = back[:Nr]
		dr[i+M2::M] = -back[Nr:][::-1] #e_r points into the other direction for negative r 
	return dr

def Dchebfft(v):
	N = len(v)-1; 
	if N==0: return 0
	x = np.cos(np.arange(0,N+1)*np.pi/N)
	ii = np.arange(0,N); iir = np.arange(1-N,0); iii = np.array(ii,dtype=int)
	V = np.hstack((v,v[N-1:0:-1])) # transform x -> theta 
	U = np.real(fft(V))
	W = np.real(ifft(1j*np.hstack((ii,[0.],iir))*U))
	w = np.zeros(N+1)
	w[1:N] = -W[1:N]/np.sqrt(1-x[1:N]**2)    # transform theta -> x     
	w[0] = sum(iii**2*U[iii])/N + .5*N*U[N]     
	w[N] = sum((-1)**(iii+1)*ii**2*U[iii])/N + .5*(-1)**(N+1)*N*U[N]
	return w

def D2chebfft(v):
	N = len(v)-1; 
	if N==0: return 0
	x = np.cos(np.arange(0,N+1)*np.pi/N)
	ii = np.arange(0,N); iir = np.arange(1-N,0); iii = np.array(ii,dtype=int)
	V  = np.hstack((v,v[N-1:0:-1])) # transform x -> theta 
	U  = np.real(fft(V))
	W1 = np.real(ifft(1j*np.hstack((ii,[0.],iir))*U))
	W2 = np.real(ifft(-(np.hstack((ii,[N],iir)))**2*U))
	w  = np.zeros(N+1)
	w[1:N] = W2[1:N]/(1-x[1:N]**2) - x[1:N]*W1[1:N]/((1-x[1:N]**2)**(3.0/2.0))    # transform theta -> x     
	w[0] = (sum(iii**2*(iii**2-1.0)*U[iii]) + 0.5*N**2*(N**2-1.0)*U[N])/(3.0*N)
	w[N] = (sum((-1)**(iii)*iii**2*(iii**2-1.0)*U[iii]) + 0.5*(-1)**N*N**2*(N**2-1.0)*U[N])/(3.0*N)
	return w

def Spec(n):#return spectral diff Matrix (periodic)
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	i = np.array( range(1,n) )
	column = np.hstack(( [0.], .5*(-1)**i/np.tan(i*h/2.) ))
	row = np.hstack(([0.], column[n-1:0:-1] ))
	D = linalg.toeplitz(column,row)
	return (D, x)

def cheb(n): #return cheb diff Matrix
	D = np.empty((n+1, n+1))
	D[0,0] = (2. * n**2 + 1. )/6.
	D[n,n] = -(2. * n**2 + 1. )/6.
	
	x = np.empty(n+1)
	for i in range(n+1):
		x[i] = np.cos((i * np.pi)/n) 
	
	for i in range(1,n):
		D[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,n+1):
		for j in range(0,n+1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == n :
					ci = 2.
				if j == 0 or j == n :
					cj = 2.
				D[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	return (D, x )

path = 'DCT-2D'
if not os.path.exists(path): #create Folders for Output State
	os.makedirs(path)

N = 101
M = 20
CellR = 1.
N2 = (N-1)/2
Nr = N2+1
PNum= M*(N2+1)

(CD, r) = cheb(N)
r = r[:N2+1]*CellR # only positive r
CD = CD*1./CellR
CDN = np.copy(CD)
CDN[0] = 0
CDN[-1] = 0
CD2 = np.dot(CD, CD)
CD2N = np.dot(CD, CDN)
R = np.diag(1./r)
rd1 = 1./r
rd2 = 1./(r**2)

#theta

M2 = M/2
(Dphi,phi) = Spec(M)
D2phi = np.dot(Dphi,Dphi)

Knots = np.empty((PNum,2))
Coord = np.empty((PNum,2))
for i in range(PNum):
	Knots[i][0] = r[i/(PNum/len(r))] * np.cos(phi[i%M])
	Knots[i][1] = r[i/(PNum/len(r))] * np.sin(phi[i%M])
	Coord[i][0] = r[i/(PNum/len(r))]
	Coord[i][1] = phi[i%M]

D1 = CD2[:N2+1,:N2+1] #no RB
D2 = np.fliplr(CD2[:N2+1,N2+1:])
E1 = CD[:N2+1,:N2+1]
E2 = np.fliplr(CD[:N2+1,N2+1:])

AA = np.vstack((np.hstack((np.zeros((M2,M2)),np.eye(M2))), np.hstack((np.eye(M2),np.zeros((M2,M2))))) )

Drad2D = (np.kron(E1,np.eye(M)) + np.kron(E2,AA))

Dphi2D = np.kron(R,Dphi )


#~ C = 1. + 0.01*np.cos( np.pi * Knots[:,0]/CellR)*(abs(Coord[:,0]-CellR)/CellR)**2


#~ d2rup = np.dot(D2rad2D,C)

S= True
if S:
	Drad2D_S = (np.kron(E1,np.eye(M)) + np.kron(E2,AA))
	D2rad2D_S = (np.kron(D1,np.eye(M)) + np.kron(D2,AA))
	C =  0.5+0.01*np.tanh(3.*(25.-Coord[:,0])/25.5)
	drC = np.dot(Drad2D_S,C)
	d2rC = np.dot(D2rad2D_S,C)
	drC_DCT = dr(C)
	
	fig, ax = plt.subplots(3, 2, figsize = (3*5,2*5))
	ax[0][0].set_title('$C$')
	ax[0][0].tricontourf(Knots[:,0],Knots[:,1],C, 500)
	ax[0][0].set_aspect('equal')

	ax[0][1].set_title('$\partial_r C$ (Full r-axis, Matrix)')
	ax[0][1].tricontourf(Knots[:,0],Knots[:,1],drC, 500)
	ax[0][1].set_aspect('equal')

	ax[1][0].set_title('$\partial_r C$ (Full r-axis, DCT)')
	ax[1][0].tricontourf(Knots[:,0],Knots[:,1],d2rC, 500)
	ax[1][0].set_aspect('equal')
	
	i=2
	ax[1][1].set_title('$\partial^2_r C$ (Full r-axis, DCT)')
	ax[1][1].plot(-r,drC[i::M],label='$\partial_r u_P$, Only r+')
	ax[1][1].plot(r[::-1],drC[i+M2::M][::-1])
	
	ax[2][0].plot(-r,C[i::M],label='u_P, r+')
	ax[2][0].plot(r[::-1],C[i+M2::M][::-1],label='u_P, r-')
	ax[2][0].legend(loc='best')
	#~ ax[2][1].plot(range(0,Nr),drC[i::M],label='$\partial_r u_P$, Full r')
	ax[2][1].plot(-r,d2rC[i::M],label='$\partial_r u_P$, Only r+')
	#~ ax[2][1].plot(r[::-1],drC[i+M2::M][::-1])
	ax[2][1].plot(r[::-1],d2rC[i+M2::M][::-1])
	ax[2][1].legend(loc='best')

	plt.tight_layout()
	plt.savefig(os.path.join(path,'Dr1.png'))
	plt.close(fig)

def drVec1(Vr):
	dr = np.empty_like(Vr)
	for i in range(M):
		dr[i::M] = 2./CellR*DchebDCT(Vr[i::M],1)
	return dr

def d2rVec1(Vr):
	dr = np.empty_like(Vr)
	for i in range(M2):
		dr[i::M] = 1./CellR**2*DchebDCT(Vr[i::M],2)
		dr[i+M2::M] = 1./CellR**2*DchebDCT(Vr[i+M2::M][::-1],2)[::-1]
	return dr

def drVec2(Vr):
	dr = np.empty_like(Vr)
	for i in range(M2):
		rr = np.hstack( (Vr[i::M], -Vr[i+M2::M][::-1] ))
		back = 1./CellR*DchebDCT(rr,1)
		dr[i::M] = back[:Nr]
		dr[i+M2::M] = back[Nr:][::-1] #e_r points into the other direction for negative r 
	return dr

def d2rVec2(Vr):
	dr = np.empty_like(Vr)
	for i in range(M2):
		rr = np.hstack( (Vr[i::M], -Vr[i+M2::M][::-1] ))
		back = 1./CellR**2*DchebDCT(rr,2)
		dr[i::M] = back[:Nr]
		dr[i+M2::M] = -back[Nr:][::-1] #e_r points into the other direction for negative r 
	return dr

Drad2D_V = (np.kron(E1,np.eye(M)) + np.kron(E2,AA))
D2rad2D_V = (np.kron(D1,np.eye(M)) + np.kron(D2,AA))
#vector
Flow = np.zeros(PNum*2)
Flow[PNum:] = np.sin(2.*np.pi*Coord[:,0]/CellR)
drp1 = drVec1(Flow[PNum:])
drp1Mat = np.dot(Drad2D_V,Flow[PNum:])
drp2 = drVec2(Flow[PNum:])
d2rpa = drVec1(drp1)
d2rpb1 = d2rVec1(Flow[PNum:])
d2rpb2 = d2rVec2(Flow[PNum:])
d2rpMat = np.dot(D2rad2D_V, Flow[PNum:])

fig, ax = plt.subplots(3, 2, figsize = (3*5,2*5))
ax[0][0].set_title('$C$')
ax[0][0].tricontourf(Knots[:,0],Knots[:,1],Flow[PNum:], 500)
ax[0][0].set_aspect('equal')

ax[0][1].set_title('$\partial_r u_p$, Matrix')
ax[0][1].tricontourf(Knots[:,0],Knots[:,1],drp1Mat, 500)
ax[0][1].set_aspect('equal')

ax[1][0].set_title('$\partial^2_r u_p$, Matrix')
ax[1][0].tricontourf(Knots[:,0],Knots[:,1],d2rpMat, 500)
ax[1][0].set_aspect('equal')

i=2
ax[1][1].set_title('flow field')
ax[1][1].plot(-r,Flow[PNum:][i::M],label='u_P, r+')
ax[1][1].plot(r[::-1],Flow[PNum:][i+M2::M][::-1],label='u_P, r-')
ax[1][1].legend(loc='best')
#~ ax[2][0].plot(range(0,Nr),drp[i::M],label='$\partial_r u_P$, Full r')
#~ ax[2][0].plot(-r,drp1[i::M],label='$\partial_r u_P$, Half r')
ax[2][0].set_title(r'$\partial_r$')
ax[2][0].plot(-r,drp2[i::M],label='DCT Full')
#~ ax[2][0].plot(-r,drp1[i::M],label='DCT Half')
#~ ax[2][0].plot(r[::-1],drp2[i+M2::M][::-1])
#~ ax[2][0].plot(-r,drp1Mat[i::M],label='$\partial_r u_P$, Mat r')
ax[2][0].plot(r[::-1],drp1Mat[i+M2::M][::-1],label='Matrix')
ax[2][0].legend(loc='best')
ax[2][1].set_title(r'$\partial^2_r$')
#~ ax[2][1].plot(-r,d2rpb1[i::M],label='$\partial_r u_P$, Only r+')
ax[2][1].plot(-r,d2rpb2[i::M],label='DCT Full')
#~ ax[2][1].plot(-r,d2rpMat[i::M],label='$\partial_r u_P$, Mat')
#~ ax[2][1].plot(r[::-1],d2rpb1[i+M2::M][::-1])
#~ ax[2][1].plot(r[::-1],d2rpb2[i+M2::M][::-1])
ax[2][1].plot(r[::-1],d2rpMat[i+M2::M][::-1],label='Matrix')
ax[2][1].legend(loc='best')

plt.tight_layout()
plt.savefig(os.path.join(path,'VecDr1.png'))
plt.close(fig)
