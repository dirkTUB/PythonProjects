import os
import numpy as np
import scipy as sp
from numpy.fft import fft,ifft
from scipy.fftpack import idct, dct
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time

def DchebDCT(v, m):
	n=len(v)
	fac = 1/np.sqrt(2*(n-1))
	U = dct(v, type=1)*fac #type 1 does not support normalization yet
	
	for i in range(m): # mth derivative
		U1 = np.copy(U)
		U[-1] = 0
		U[-2] = 0
		for i in range(len(U)-2, 0, -1):
			U[i-1] = 2*i*U1[i] + U[i+1]
	w = idct(U, type=1)*fac
	return w

def Dchebfft(v):
	N = len(v)-1; 
	if N==0: return 0
	x = np.cos(np.arange(0,N+1)*np.pi/N)
	ii = np.arange(0,N); iir = np.arange(1-N,0); iii = np.array(ii,dtype=int)
	V = np.hstack((v,v[N-1:0:-1])) # transform x -> theta 
	U = np.real(fft(V))
	W = np.real(ifft(1j*np.hstack((ii,[0.],iir))*U))
	w = np.zeros(N+1)
	w[1:N] = -W[1:N]/np.sqrt(1-x[1:N]**2)    # transform theta -> x     
	w[0] = sum(iii**2*U[iii])/N + .5*N*U[N]     
	w[N] = sum((-1)**(iii+1)*ii**2*U[iii])/N + .5*(-1)**(N+1)*N*U[N]
	return w

def D2chebfft(v):
	N = len(v)-1; 
	if N==0: return 0
	x = np.cos(np.arange(0,N+1)*np.pi/N)
	ii = np.arange(0,N); iir = np.arange(1-N,0); iii = np.array(ii,dtype=int)
	V  = np.hstack((v,v[N-1:0:-1])) # transform x -> theta 
	U  = np.real(fft(V))
	W1 = np.real(ifft(1j*np.hstack((ii,[0.],iir))*U))
	W2 = np.real(ifft(-(np.hstack((ii,[N],iir)))**2*U))
	w  = np.zeros(N+1)
	w[1:N] = W2[1:N]/(1-x[1:N]**2) - x[1:N]*W1[1:N]/((1-x[1:N]**2)**(3.0/2.0))    # transform theta -> x     
	w[0] = (sum(iii**2*(iii**2-1.0)*U[iii]) + 0.5*N**2*(N**2-1.0)*U[N])/(3.0*N)
	w[N] = (sum((-1)**(iii)*iii**2*(iii**2-1.0)*U[iii]) + 0.5*(-1)**N*N**2*(N**2-1.0)*U[N])/(3.0*N)
	return w


path = 'DCT'
if not os.path.exists(path): #create Folders for Output State
	os.makedirs(path)

N=64
x = np.empty(N+1)
for i in range(N+1):
	x[i] = np.cos((i * np.pi)/N)

TEST = np.cos( x* np.pi)
TEST1 = -np.sin( x* np.pi) *np.pi
TEST2 = -np.cos( x* np.pi) *np.pi**2

fftF1 = Dchebfft(TEST)
fftF2 = D2chebfft(TEST)
dctD1 = DchebDCT(TEST,1)
dctD2 = DchebDCT(TEST,2)

f, ax = plt.subplots(2, 2,figsize=(2*5,2*5))
ax[0][0].plot(x,TEST)
ax[0][0].plot(x,TEST1)
ax[0][0].plot(x,fftF1)
ax[0][0].plot(x,dctD1)
ax[0][1].plot(x,TEST)
ax[0][1].plot(x,TEST2)
ax[0][1].plot(x,fftF2)
ax[0][1].plot(x,dctD2)
ax[1][0].plot(x,TEST1-fftF1)
ax[1][0].plot(x,TEST1-dctD1)
ax[1][1].plot(x,TEST2-fftF2)
ax[1][1].plot(x,TEST2-dctD2)

plt.tight_layout()
plt.savefig(os.path.join(path,'Diff.png'),bbox_inches='tight')
plt.close(f)

start = time.clock()
for i in range(1000):
	Dchebfft(TEST)
print 'First deriv fft:'+str(time.clock()-start)

start = time.clock()
for i in range(1000):
	D2chebfft(TEST)
print 'Seconf deriv fft:'+str(time.clock()-start)

start = time.clock()
for i in range(1000):
	DchebDCT(TEST,1)
print 'First deriv DCT:'+str(time.clock()-start)

start = time.clock()
for i in range(1000):
	DchebDCT(TEST,2)
print 'Second deriv DCT:'+str(time.clock()-start)
