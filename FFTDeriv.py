# CHEBFFT  Chebyshev differentiation via FFT. Simple, not optimal.  
#          If v is complex, delete "real" commands.
import numpy as np
from numpy.fft import fft,ifft
from scipy.fftpack import idct, dct

def DchebDCT(v, m):
	n=len(v)
	fac = 1/np.sqrt(2*(n-1))
	U = dct(v, type=1)*fac #type 1 does not support normalization yet
	
	for i in range(m): # mth derivative
		U1 = np.copy(U)
		U[-1] = 0
		U[-2] = 0
		for i in range(len(U)-2, 0, -1):
			U[i-1] = 2*i*U1[i] + U[i+1]
	w = idct(U, type=1)*fac
	return w

def D1fft(v, k1):
	return np.imag(np.fft.ifft(k1*np.fft.fft(v)))
	
def D2fft(v, k2):
	U = np.fft.fft(v)
	return np.real(np.fft.ifft(k2*U))


def SpecCoef(n, m, rr):
	dk1 = np.empty(m)
	dk2 = np.empty(m)
	for i in range(m/2):
		dk1[i] = i
	for i in range(m/2+1):
		dk2[i] = i
	for i in range(m/2+1,m):
		dk1[i] = i-m
		dk2[i] = i-m
	
	if m %2 ==0:
		dk1[m/2] = 0
	else:
		filt[m /2] = 1
	
	dk1r = np.empty((n,m))
	dk2r = np.empty((n,m))
	
	for i in range(n):
		dk1r[i] = dk1/rr[i]
		dk2r[i] = -(dk2/rr[i])**2
	
	return (dk1r, dk2r)
