import os
import numpy as np
import scipy as sp
from numpy.fft import fft,ifft
from scipy.fftpack import idct, dct
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from FFTDeriv import D1fft, D2fft
import scipy.sparse.linalg as spla
from scipy import linalg

def Spec(n):#return spectral diff Matrix (periodic)
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	i = np.array( range(1,n) )
	column = np.hstack(( [0.], .5*(-1)**i/np.tan(i*h/2.) ))
	row = np.hstack(([0.], column[n-1:0:-1] ))
	D = linalg.toeplitz(column,row)
	return (D, x)

path = 'FFTFilter'
if not os.path.exists(path): #create Folders for Output State
	os.makedirs(path)

m=50
(Dphi,phi) = Spec(m)

alpha = -np.log(np.finfo(float).eps)
print alpha
filt = np.empty(m)
dk1 = np.empty(m)
dk2 = np.empty(m)
for i in range(m/2):
	dk1[i] = i
	
for i in range(m/2+1):
	dk2[i] = i
	filt[i] = np.exp(-alpha*(float(i)/float(m)) **(4)  )
for i in range(m/2+1,m):
	dk1[i] = i-m
	dk2[i] = i-m
filt[-m/2-1:] = filt[:m/2+1][::-1]
if m %2 ==0:
	dk1[m/2] = 0
else:
	filt[m /2] = 1

print filt
print len(filt)
TEST = np.cos( phi)*np.cos( 2*phi)
TEST1 = -np.sin( phi)

fftF1 = D1fft(TEST, dk1)
fftF2 = D1fft(TEST, dk1*filt)


f, ax = plt.subplots(1, 2,figsize=(2*5,1*5))
#~ ax[0].plot(phi,TEST)
#~ ax[0].plot(phi,TEST1)
ax[0].plot(phi,fftF1)
ax[0].plot(phi,fftF2)

#~ ax[1].plot(phi,fftF1-fftF2)
ax[1].plot(phi,filt)

plt.legend()
plt.tight_layout()
plt.savefig(os.path.join(path,'Diff.png'),bbox_inches='tight')
plt.close(f)
