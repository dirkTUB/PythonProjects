import os
import numpy as np
from scipy import linalg
import scipy.sparse.linalg as spla
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla
from scipy.interpolate import griddata
from mpl_toolkits.axes_grid1 import make_axes_locatable
from FFTDeriv import D1fft, D2fft, DchebDCT, SpecCoef

def Spec(n):
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	i = np.array( range(1,n) )
	column = np.hstack(( [0.], .5*(-1)**i/np.tan(i*h/2.) ))
	row = np.hstack(([0.], column[n-1:0:-1] ))
	D = linalg.toeplitz(column,row)
	return (D, x)

def cheb(N, x0 = 0.1, x1 = 1):
	D = np.empty((N+1, N+1))
	D[0,0] = (2. * N**2 + 1. )/6.
	D[N,N] = -(2. * N**2 + 1. )/6.
	
	x = np.empty(N+1)
	for i in range(N+1):
		x[i] = np.cos((i * np.pi)/N) 
	
	for i in range(1,N):
		D[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,N+1):
		for j in range(0,N+1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == N :
					ci = 2.
				if j == 0 or j == N :
					cj = 2.
				D[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	
	y = np.empty(N+1)
	for i in range(N+1):
		y[i] = (x[i]+1 )/2 * x1 + x0
	
	return (2./(x1-x0) *D, y)

def PlotFigure(c, run):
	fig, (ax, ax2) = plt.subplots(1, 2)
	ax.set_aspect('equal')
	ax2.set_aspect('equal')
	im = ax.tricontourf(Knots[:,0],Knots[:,1], c[:PNum], 150,cmap='cool')
	divider = make_axes_locatable(ax)
	cax = divider.append_axes("right", size="8%", pad=0.05)
	cbar = plt.colorbar(im, cax=cax)
	#~ im2 = ax2.tricontourf(Knots[:,0],Knots[:,1], c[PNum:], 150,cmap='cool')
	#~ divider2 = make_axes_locatable(ax2)
	#~ cax2 = divider2.append_axes("right", size="8%", pad=0.05)
	#~ cbar2 = plt.colorbar(im2, cax=cax2)

	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','Heat_'+str(run)+'.png'),bbox_inches='tight')
	plt.close(plt.gcf())

def Div(v):
	(Vr, Vp) = (v[:PNum], v[PNum:])
	return dr(Vr)+Rad(np.copy(Vr),rd1) + dp(Vp)

def d2rN(Vr):
	return dr( drN(Vr))
	
def drN(Vr):
	dr = np.empty_like(Vr)
	for i in range(M):
		dr[i::M] = 1./CellR*DchebDCT(Vr[i::M],1)
	dr[:M] = 0.
	return dr

def dr(Vr):
	dr = np.empty_like(Vr)
	for i in range(M):
		dr[i::M] = 1./CellR*DchebDCT(Vr[i::M],1)
	return dr
	
def Rad(v,r): # this function modifies the value given to it, use copy
	vp0 = np.reshape(v,(N1r,M))
	for i in range(N1r):
		vp0[i] *=r[i]
	return vp0.flatten()

def d2p(Vp): #1/r^2 is aready included in the wavenumber
	vp0 = np.reshape(Vp,(N1r,M))
	dp = np.empty((N1r,M))
	for i in range(N1r):
		dp[i,:] = D2fft(vp0[i,:], k2[i])
	return dp.flatten()

CellR = 75
InitialR = 2
dt = 0.01
Du = 200.
Dw = 0.05
EndTime = 100
SaveTime = 5.
MinR = 0.1
Bound = 1 #0 = Dir, 1 = no Flux, 2: r no-flux, l = Dir0

path = 'FHN'
Steps = int(EndTime/dt)+1
SaveStep = int(SaveTime/dt)
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))

#radius
Nr = 21 #odd
N1r = Nr+1

#radius
(CD,r) = cheb(Nr, MinR, CellR-MinR)
CDN = np.copy(CD)
CDN[0,:] = 0
CDD = np.copy(CD)
CDD[-1:]= 0
CDD[:,-1]= 0
CD2 = np.dot(CD, CD)
CD2N = np.dot(CD, CDN)
CD2D = np.dot(CD, CDD)
CD2D[-1:]= 0
CD2D[:,-1]= 0
R = np.diag(1./r)
rd1 = 1./r
rd2 = 1./(r**2)

#phi
M = 20 #even
(Dphi,phi) = Spec(M)
D2phi = np.dot(Dphi,Dphi)
(k1, k2) = SpecCoef(N1r, M, r)
PNum= M*N1r

Knots = np.empty((PNum,2))
Coord = np.empty((PNum,2))
for i in range(PNum):
	Knots[i][0] = r[i/(PNum/len(r))] * np.cos(phi[i%M])
	Knots[i][1] = r[i/(PNum/len(r))] * np.sin(phi[i%M])
	Coord[i][0] = r[i/(PNum/len(r))]
	Coord[i][1] = phi[i%M]

Drad2D = np.kron(CD,np.eye(M))
D2rad2D = np.kron(CD2,np.eye(M))
Drad2DN = np.kron(CDN,np.eye(M))
D2rad2DN = np.kron(CD2N,np.eye(M))
R2D = np.kron(R,np.eye(M))
Dphi2D = np.kron(R,Dphi )
D2phi2D =  np.kron(np.dot(R,R),D2phi)

Diff =  (np.kron(CD2N + np.dot(R,CDN),np.eye(M))+ np.kron(np.dot(R,R),D2phi))
A = np.eye(PNum) - dt*Du*Diff

#~ A = np.zeros( (2*PNum,2*PNum))
#~ A[:PNum,:PNum] += np.eye(PNum)
#~ A[:PNum,:PNum] += -dt*Du*Diff
#~ A[PNum:,PNum:] += np.eye(PNum)


Weight = np.zeros( PNum )
for j in range(len(r)):
	if j == 0:
		Weight[j*M:(j+1)*M] = (r[0]**2 - ( (r[0]+ r[1]  )/2.)**2) * np.pi/M
	
	elif j == len(r)-1:
		Weight[j*M:(j+1)*M] = ((r[-1] +r[-2])/2 ) **2 * np.pi/M
	else:
		Weight[j*M:(j+1)*M] = (( (r[j]+ r[j-1]  )/2.)**2 - ( (r[j]+ r[j+1]  )/2.)**2 )* np.pi/M

#~ Chem = np.zeros(2*PNum)
xs,ys = np.mgrid[-CellR:CellR:250j, -CellR:CellR:250j]

#~ for i in range(PNum):
	#~ rad = (Knots[i,0])**2 + (Knots[i,1]+40.)**2 
	#~ Chem[i] 	= 0.5+0.5*np.tanh(3*(3000. - (Coord[i,0])**2)/20000.);
	#~ Chem[i+PNum] = 0.3+0.3*np.tanh(3*(3000. - rad)/20000.);

def applyChem(vv):
	(U, W) = (vv[:PNum], vv[PNum:])
	
	uu = U - dt*Du*np.dot(Diff, U)
	 #~ - dt*( U*(U-a)*(1.-U)  - W + I )
	 #~ - dt*Du*(d2rN(U)+Rad(drN(U),rd1) + d2p(U) ) ++ Div(np.hstack( (wr*C, wp*C )))
	ww = W - dt*eps*(U - g*W)
	

	
	return np.hstack( (uu, ww))

def applyChemMat(v):
	aa = np.zeros_like(v)
	for i in range(len(aa)):
		aa[:,i] = applyChem(v[:,i])
	return aa

I = 0.0
a = 0.002
g = 0.5

eps = 0.0024804
tol= 1e-8

ChemOP = spla.LinearOperator((2*PNum,2*PNum),matvec=applyChem,matmat=applyChemMat,dtype=float)
ChemOPMat = ChemOP.matmat(np.eye(2*PNum))
ILUC = spla.splu(ChemOPMat)
def applyCILUPrec(v):        # Define action of preconditioner
	return ILUC.solve(v)
PC = spla.LinearOperator((2*PNum,2*PNum),matvec=applyCILUPrec,dtype=float)

#~ MC2 = spla.spilu(A)
#~ MC_x = lambda x: MC2.solve(x)
#~ MCPre = spla.LinearOperator((2*PNum, 2*PNum), MC_x)
Chem = 0.5*(np.sign(Knots[:,0])+1) *1.8
Ntot = 15000
ka = 0.01
kb = 10.
kc = 10.
kka = 1.

MC2 = spla.spilu(A)
MC_x = lambda x: MC2.solve(x)
MCPre = spla.LinearOperator((PNum, PNum), MC_x)

print np.dot(Weight,Chem)
PlotFigure(Chem, -1)
for step in range(Steps):
	
	tot =  (Ntot - np.dot(Weight,Chem) )
	#~ print tot
	#~ print np.dot(Weight,Chem)
	if tot > 3.5:
		tot = 3.5
	if tot < -3.5:
		tot = -3.5
	ChemNew,info = spla.gmres(A,Chem + dt*(kb *(Chem**2/(kka**2+ Chem**2) +ka) *tot - kc * Chem), M=MCPre, tol = tol)
	
	#~ Expl = Dcon*(d2rN(C)+Rad(drN(C),rd1) + d2p(C) ) - Div(np.hstack( (wr*C,wp*C )))
	#~ Reak = dt* np.hstack( (Chem[:PNum]*(Chem[:PNum]-a)*(1.-Chem[:PNum]) - Chem[PNum:] + 0.5, eps*(Chem[:PNum] - g*Chem[PNum:]) ) )
	#~ ChemNew, info = spla.gmres(ChemOP,Chem, M=PC, x0=Chem, tol=tol)
	#~ print ChemNew
	#~ ChemNew = np.copy(Chem)
	if step % SaveStep == 0:
		run = step / SaveStep
		
		PlotFigure(ChemNew, run)
		print str(run)
	ChemNew, Chem = Chem, ChemNew
