import os
import numpy as np
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla


def FD2(x): #Second derivative with FD on general grid
	DFD2 = np.zeros((N1,N1))
	
	DFD2[0][0] = 1./abs(x[0]-x[1])**2
	DFD2[0][1] = -1./abs(x[0]-x[1])**2
	DFD2[-1][-1] = -1./abs(x[-1]-x[-2])**2
	DFD2[-1][-2] = 1./abs(x[-1]-x[-2])**2
	
	for i in range(1,len(DFD2)-1):
		x1 = abs(x[i-1]-x[i])
		x2 = abs(x[i+1]-x[i])
		DFD2[i][i] = - 2./(x1 * x2)
		DFD2[i][i+1] = 2./(x2*(x2 + x1))
		DFD2[i][i-1] = 2./(x1*(x1 + x2))
	
	return DFD2
	
def cheb(N, x0 = -1., x1 = 1.): #return cheb diff Matrix
	D = np.empty((N+1, N+1))
	D[0,0] = (2. * N**2 + 1. )/6.
	D[N,N] = -(2. * N**2 + 1. )/6.
	
	x = np.empty(N1)
	for i in range(N1):
		x[i] = np.cos((i * np.pi)/N) 
	
	for i in range(1,N):
		D[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,N1):
		for j in range(0,N1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == N :
					ci = 2.
				if j == 0 or j == N :
					cj = 2.
				D[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	
	y = np.empty(N1)
	for i in range(N1):
		y[i] = (x[i]+1)/2 * x1
	
	return (2./(x1-x0) *D, y)
	
def BuildMatrix(D2):
	Temp = np.eye(N1) - dt * Dcon * D2
	
	if bound == 0:
		return Temp[1:-1,1:-1]
	elif bound == 1:
		return Temp
	else:
		return Temp[1:,1:]

def PlotFigure(x, C, run):
	f, ax3 = plt.subplots(2, 1)
	ax3[0].plot(x,C)
	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','Heat_'+str(run)+'.png'),bbox_inches='tight')
	plt.close(f)

CellR = 2.
dt = 0.01
Dcon = 1.
EndTime = 5.
SaveTime = 0.1

bound = 1 #0= dirch, 1 = no flux , 2 = r: d0, l: no flux
Prec = 3 #0 = no, 1 = FD, 2 = ILU, 3 = FD-ILU

Step = int(EndTime/dt)+1
SaveStep = int(SaveTime/dt)

N = 41
N1 = N+1
(D, x) = cheb(N, 0, CellR)
CD2 = np.dot(D, D)

path = 'HeatChebPrec'
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))

if bound == 0:
	Diff = CD2
	X = x[1:N]
elif bound == 1:
	DN = np.copy(D)
	DN[0] = np.zeros(N1)
	DN[N] = np.zeros(N1)
	Diff = np.dot(D, DN)
	X = x
elif bound == 2:
	DN = np.copy(D)
	DN[N] = np.zeros(N1)
	Diff = np.dot(D, DN)
	X = x[1:]
	
A = BuildMatrix(Diff)

start = time.clock()

print 'Cond :'+str( np.linalg.cond(A) )
if Prec  == 1:
	def applyPrec(v):   # Define action of preconditioner
		return spla.spsolve(-csr_matrix(AFD),v)
	AFD = BuildMatrix(FD2(x))
	M = spla.LinearOperator((len(A),len(A)),matvec=applyPrec, dtype=float)
elif Prec == 2:
	M22 = spla.spilu(A)
	M_x = lambda x: M22.solve(x)
	M = spla.LinearOperator((len(A), len(A)), M_x)
elif Prec == 3:
	AFD = BuildMatrix(FD2(x))
	M22 = spla.spilu(AFD)
	M_x = lambda x: M22.solve(x)
	M = spla.LinearOperator((len(A), len(A)), M_x)

C = 0.5+0.5*np.tanh(3*(0.2 - (X-CellR/2.)**2)/0.5);
S = np.zeros(len(C))
#~ S[2*N1/5:3*N1/5] = 0.25

PlotFigure(X, C, -1)

for t in range(Step):
	if Prec > 0:
		CNew, info = spla.lgmres(A,C + S*dt, M=M, tol=1e-10)
	else:
		CNew, info = spla.lgmres(A,C+ S*dt, tol=1e-10)
	
	if t % SaveStep == 0 or t == Step-1:
		run = t / SaveStep
		print( str(run)+'_'+str(time.clock() - start) )
		PlotFigure(X, CNew, run)
		
	CNew, C = C, CNew
