import os
import numpy as np
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla


def FD2(x): #Second derivative with FD on general grid
	DFD2 = np.zeros((N1,N1))
	
	DFD2[0][0] = 1./abs(x[0]-x[1])**2
	DFD2[0][1] = -1./abs(x[0]-x[1])**2
	DFD2[-1][-1] = -1./abs(x[-1]-x[-2])**2
	DFD2[-1][-2] = 1./abs(x[-1]-x[-2])**2
	
	for i in range(1,len(DFD2)-1):
		x1 = abs(x[i-1]-x[i])
		x2 = abs(x[i+1]-x[i])
		DFD2[i][i] = - 2./(x1 * x2)
		DFD2[i][i+1] = 2./(x2*(x2 + x1))
		DFD2[i][i-1] = 2./(x1*(x1 + x2))
	
	return DFD2

def chebRadau(N, CellR):
	D = np.empty((N+1, N+1))
	D[0,0] = (N+1.)*N/3.
	
	x = np.empty(N+1)
	for i in range(N+1):
		x[i] = np.cos(2. * i *np.pi/(2.*N+1.))
	
	for i in range(1,N+1):
		D[i,i] = -1./(2.*(1.-x[i]**2))

	for i in range(0,N+1):
		for j in range(0,N+1):
			if i != j:
				ci = np.sqrt(2./(1.+x[i]))
				cj = np.sqrt(2./(1.+x[j]))
				if i == 0 :
					ci = 2.
				if j == 0 :
					cj = 2.
				D[i,j] =(-1)**j*(-1)**i * ci/cj * 1./ (x[i] - x[j])
	
	x = x[::-1]
	D = np.fliplr(np.flipud(D))
	
	y = np.empty(N+1)
	for i in range(N+1):
		y[i] = (x[i]+1)/2 * CellR
	
	return (1./CellR *D, x, y)

def cheb(N, x0 = -1., x1 = 1.): #return cheb diff Matrix
	D = np.empty((N+1, N+1))
	D[0,0] = (2. * N**2 + 1. )/6.
	D[N,N] = -(2. * N**2 + 1. )/6.
	
	x = np.empty(N1)
	for i in range(N1):
		x[i] = np.cos((i * np.pi)/N) 
	
	for i in range(1,N):
		D[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,N1):
		for j in range(0,N1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == N :
					ci = 2.
				if j == 0 or j == N :
					cj = 2.
				D[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	
	y = np.empty(N1)
	for i in range(N1):
		y[i] = (x[i]+1)/2 * x1
	
	return (2./(x1-x0) *D, y)
	
def PlotFigure(x, C, run):
	f, ax3 = plt.subplots(3, 1)
	ax3[0].plot(x,C)
	ax3[1].plot(x,np.dot(Dr,C))
	ax3[2].plot(x,np.dot(D2,C))
	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','Heat_'+str(run)+'.png'),bbox_inches='tight')
	plt.close(f)

path = 'HeatChebRadau'
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))

N = 41
N1 = N+1
CellR = 1.
dt = 0.01
Dcon =0.01
EndTime = 10
SaveTime = 0.1

Bound = 1 #0= dirch, 1 = no flux , 2 = r: d0, l: no flux
Prec = 2 #0 = no, 1 = FD, 2 = ILU, 3 = FD-ILU

Step = int(EndTime/dt)+1
SaveStep = int(SaveTime/dt)


(DGR,x,r) = chebRadau(N, CellR)

R = np.diag(1./(1+x) *1./CellR)

DN = np.copy(DGR)
DN[-1,:] = 0
D2 = np.dot(DGR, DGR)
D2N = np.dot(DGR, DN)

#~ Weight = linalg.pinv2(DGR[:N,:N] )[0,:]
Weight = 2./(np.array(range(0,N+1,2))-1.)


if Bound == 0:
	Dr = 2*DGR
	D2r = 4.*(D2 + np.dot(R,Dr))
	D2r[-1:] = 0
	D2r[:,-1] = 0
if Bound == 1:
	Dr = 2*DGR
	D2r = 4.*(D2 + np.dot(R,Dr))

#~ print Dr
#~ print D2r
#~ exit(1)
A = np.eye(N1) - dt * Dcon * D2r

if Bound == 1:
	A[-1:] = Dr[-1:]

start = time.clock()

print 'Cond :'+str( np.linalg.cond(A) )
if Prec  == 1:
	def applyPrec(v):   # Define action of preconditioner
		return spla.spsolve(-csr_matrix(AFD),v)
	AFD = BuildMatrix(FD2(x))
	M = spla.LinearOperator((len(A),len(A)),matvec=applyPrec, dtype=float)
elif Prec == 2:
	M22 = spla.spilu(A)
	M_x = lambda x: M22.solve(x)
	M = spla.LinearOperator((len(A), len(A)), M_x)
elif Prec == 3:
	AFD = BuildMatrix(FD2(x))
	M22 = spla.spilu(AFD)
	M_x = lambda x: M22.solve(x)
	M = spla.LinearOperator((len(A), len(A)), M_x)

C = 0.5+0.5*np.tanh(3*( - (r[::-1]-CellR)**2)/0.5);
 #~ 0.2*np.cos( (2*np.pi * r[::-1])/CellR+np.pi)
S = np.zeros(len(C))
b = np.zeros(len(C))
#~ S = 0.02-0.02*np.cos( (2*np.pi * r[::-1])/CellR)

PlotFigure(r, C, -1)

for t in range(Step):
	#~ b[:-1] = C[:-1]
	if Prec > 0:
		if t < 100:
			CNew, info = spla.gmres(A,C + S*dt, M=M, tol=1e-10)
		else: 
			CNew, info = spla.gmres(A,C, M=M, tol=1e-10)
	else:
		CNew, info = spla.gmres(A,b+ S*dt, tol=1e-10)
	
	if t % SaveStep == 0 or t == Step-1:
		run = t / SaveStep
		print( str(run)+'_'+str(time.clock() - start) +' '+str(np.dot(Weight, C[::2]))) +' '+str(C[-1])
		PlotFigure(r, CNew, run)
		
	CNew, C = C, CNew
