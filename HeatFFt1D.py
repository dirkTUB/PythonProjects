import os
import numpy as np
import scipy as sp
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla
from scipy.interpolate import griddata
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.fftpack import idct, dct
from chebfft import Dchebfft, D2chebfft, Dfft, D2fft

def Spec(n):
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	i = np.array( range(1,n) )
	column = np.hstack(( [0.], .5*(-1)**i/np.tan(i*h/2.) ))
	row = np.hstack(([0.], column[n-1:0:-1] ))
	D = linalg.toeplitz(column,row)
	return (D, x)

def cheb(n): #return cheb diff Matrix
	DTemp = np.empty((n+1, n+1))
	DTemp[0,0] = (2. * n**2 + 1. )/6.
	DTemp[n,n] = -(2. * n**2 + 1. )/6.
	
	x = np.empty(n+1)
	for i in range(n+1):
		x[i] = np.cos((i * np.pi)/n) 
	
	for i in range(1,n):
		DTemp[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,n+1):
		for j in range(0,n+1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == n :
					ci = 2.
				if j == 0 or j == n :
					cj = 2.
				DTemp[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	return (DTemp, x)

def PlotFigure(x, C,DC, run):
	#~ c = np.hstack( C))
	fig, ax = plt.subplots(1, 1, figsize = (1*5,5))
	
	ax.plot(C)
	ax.plot(DC)
	
	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','State{0:04d}.png'.format(run)),bbox_inches='tight')
	plt.close(plt.gcf())

CellR = 75
InitialR = 2
dt = 0.001
Dcon = 1
EndTime = 5.
SaveTime = 0.5
Bound = 1 #0 = Dir, 1 = no Flux, 2: r no-flux, l = Dir0

path = 'HeatFFT1D'
Steps = int(EndTime/dt)+1
SaveStep = int(SaveTime/dt)
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))
if not os.path.exists(os.path.join(path,'FFT')): #create Folders for Output State
	os.makedirs(os.path.join(path,'FFT'))

#phi
M = 30 #even
bound='per'
if bound=='per':
	(Dphi,phi) = Spec(M)
	Num = M
elif bound=='Dir':
	(Dphi,phi) = cheb(M)
	Num = M+1


#~ A =np.eye(Num) - dt * Dcon * D2phi
#~ A =np.eye(PNum)
#~ print np.linalg.matrix_rank(A)
#~ M22 = spla.splu(A)
#~ M_x = lambda x: M22.solve(x)
#~ MPre2 = spla.LinearOperator((len(A), len(A)), M_x)

C = np.empty(Num)


if bound=='per':
	C = 1.+np.sin(phi)
	d2fft = np.empty(M)
	for i in range(M/2):
		d2fft[i] = i
	for i in range(1,M/2):
		d2fft[M-i] = -i
	k2= -d2fft**2
	#~ d2fft = np.empty(M)
	#~ for i in range(M/2):
		#~ d2fft[i] = i
	#~ for i in range(1,M/2):
		#~ d2fft[M-i] = -i
	#~ k2 = np.exp(-Dcon *dt*d2fft**2)
	
	def D2fft(v):
		U = np.real(np.fft.fft(v))
		return np.real(np.fft.ifft(k2*U))
elif bound=='Dir':
	C = 1.+np.cos(phi*np.pi)
	def D2fft(v):
		return Dchebfft(Dchebfft(v))

def counter(xk):
    print("Krylov residual:\t%10.3e" % xk)

def applyDFBC(v):
	
	w=D2fft(v)
	if bound=='Dir':
		w[0] = 0
		w[-1] = 0
		#~ w = v - dt*w
	return v + dt*w

def applyMat(v):
	a = np.empty_like(v)
	for i in range(len(a)):
		a[:,i] = applyDFBC(v[:,i])
	return a

FFTOP = spla.LinearOperator((Num,Num),matvec=applyDFBC,matmat=applyMat,dtype=float)
DFBCLMat = FFTOP.matmat(np.eye(Num))
print('Matrix rank:',np.linalg.matrix_rank(DFBCLMat))
print('Matrix cond:',np.linalg.cond(DFBCLMat))
#~ exit(1)
#~ exit(1)

#~ DFBCLMat = DFBCL.matmat(np.zeros((M,M)))
#~ DFBCLMat = FFTOP.matmat(np.eye(M))
#~ DFBCLMat = DFBCL.matmat(np.zeros((M,M)))
PlotFigure(phi, C, FFTOP*C, -1)

print( C )
#~ print
for step in range(Steps):
	ILU = spla.spilu(csr_matrix(FFTOP.matmat(np.eye(Num))))
	def applyILUPrec(v):        # Define action of preconditioner
		return ILU.solve(v)
	PILU = spla.LinearOperator((M,M),matvec=applyILUPrec,dtype=float)
	
	#~ CNew = np.linalg.solve(DFBCLMat	,C)
	#~ CNew, info   = spla.gmres(DFBCLMat	,C, x0=C, M=PILU, callback=counter)
	CNew = FFTOP*C
	#~ -dt*FFTOP*C
	
	if step % SaveStep == 0:
		run = step / SaveStep
		PlotFigure(phi, CNew,FFTOP*C, run)
		#~ print str(run)
		#~ print CNew
		#~ print info

	CNew, C = C, CNew
	#~ exit(1)
