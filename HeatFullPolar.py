import os
import numpy as np
import scipy as sp
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla
from scipy.interpolate import griddata
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pyamg as pa
from scipy.fftpack import idct, dct

def Spec(n):
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	i = np.array( range(1,n) )
	column = np.hstack(( [0.], .5*(-1)**i/np.tan(i*h/2.) ))
	row = np.hstack(([0.], column[n-1:0:-1] ))
	D = linalg.toeplitz(column,row)
	return (D, x)

def chebRadau(N, CellR):
	x0 = -1. *CellR
	x1 = 1. *CellR
	D = np.empty((N+1, N+1))
	D[0,0] = (N+1.)*N/3.
	
	x = np.empty(N+1)
	for i in range(N+1):
		x[i] = np.cos(2. * i *np.pi/(2.*N+1.))
	
	for i in range(1,N+1):
		D[i,i] = -1./(2.*(1.-x[i]**2))

	for i in range(0,N+1):
		for j in range(0,N+1):
			if i != j:
				ci = np.sqrt(2./(1.+x[i]))
				cj = np.sqrt(2./(1.+x[j]))
				if i == 0 :
					ci = 2.
				if j == 0 :
					cj = 2.
				D[i,j] =(-1)**j*(-1)**i * ci/cj * 1./ (x[i] - x[j])
	
	#~ x = x[::-1]
	#~ D = np.fliplr(np.flipud(D))
	
	y = np.empty(N+1)
	for i in range(N+1):
		y[i] = (x[i]+1)/2 * x1
	
	return (2./(x1-x0) *D, y, x)

def cheb(N, x0 = 0.1, x1 = 1):
	D = np.empty((N+1, N+1))
	D[0,0] = (2. * N**2 + 1. )/6.
	D[N,N] = -(2. * N**2 + 1. )/6.
	
	x = np.empty(N+1)
	for i in range(N+1):
		x[i] = np.cos((i * np.pi)/N) 
	
	for i in range(1,N):
		D[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,N+1):
		for j in range(0,N+1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == N :
					ci = 2.
				if j == 0 or j == N :
					cj = 2.
				D[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	
	y = np.empty(N+1)
	for i in range(N+1):
		y[i] = (x[i]+1 )/2 * x1 + x0
	
	return (2./(x1-x0) *D, y)

def PlotFigure(rad, the, C, Dphi, D2, run):
	fig, (ax, ax2, ax3) = plt.subplots(1, 3, figsize = (3*5,5))
	ax.set_title('State')
	im = ax.tricontourf(Knots[:,0],Knots[:,1],C, 500)
	ax.set_aspect('equal')
	divider = make_axes_locatable(ax)
	cax = divider.append_axes("right", size="8%", pad=0.05)
	cbar = plt.colorbar(im, cax=cax)
	
	ax2.set_title('dr')
	im2 = ax2.tricontourf(Knots[:,0],Knots[:,1],np.dot(Dr2D,C), 500)
	ax2.set_aspect('equal')
	divider2 = make_axes_locatable(ax2)
	cax2 = divider2.append_axes("right", size="8%", pad=0.05)
	cbar2 = plt.colorbar(im2, cax=cax2)
	
	ax3.set_title('d2r')
	#~ im3 = ax3.tricontourf(Knots[:,0],Knots[:,1],np.dot(D2r2D,C), 500)
	im3 = ax3.plot(np.hstack( (C[::M], C[M/2::M][::-1]) ))
	#~ ax3.set_aspect('equal')
	#~ divider3 = make_axes_locatable(ax3)
	#~ cax3 = divider3.append_axes("right", size="8%", pad=0.05)
	#~ cbar3 = plt.colorbar(im3, cax=cax3)

	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','State{0:04d}.png'.format(run)),bbox_inches='tight')
	plt.close(plt.gcf())

CellR = 1.
dt = 0.01
Dcon = 1.
EndTime = 5.
SaveTime = 0.1
Bound = 1 #0 = Dir, 1 = no Flux, 2: r no-flux, l = Dir0

path = 'HeatFullPolar'
Steps = int(EndTime/dt)+1
SaveStep = int(SaveTime/dt)
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))
if not os.path.exists(os.path.join(path,'FFT')): #create Folders for Output State
	os.makedirs(os.path.join(path,'FFT'))

#radius
Nr = 51 #odd
N1r = Nr+1 #odd

(DGR,r, x) = chebRadau(Nr, CellR)

R = np.diag(1./r)

DN = np.copy(2*DGR)
DN[0,:] = 0
D2 = np.dot(DGR, DGR)
D2N = np.dot(DGR, DN)


if Bound == 0:
	Dr = 2.*DGR
	D2r = 4.*(D2 + np.dot(R,DGR))

	D2r[:M,:] = 0
	D2r[:,:M] = 0
elif Bound == 1:
	Dr = 2*DN
	D2r = 4.*(D2N + np.dot(R,Dr))

#phi
M = 10 #even
(Dphi,phi) = Spec(M)
D2phi = np.dot(Dphi,Dphi)

D2r2D = np.kron(D2r,np.eye(M))
Dr2D = np.kron(Dr,np.eye(M))

#construct Matrices
#~ Dr2d = np.kron(CDN,np.eye(M))
#~ D2r2d = np.kron(CD2N,np.eye(M))
#~ AdvRad = -(np.kron(CDN,np.eye(M)))*50.*dt
#~ AdvPhi = np.kron(np.eye(len(r)),Dphi )*dt*0.1


L = D2r2D  + np.kron(np.dot(R,R),D2phi)

PNum = N1r*M
Knots = np.empty((PNum,2))
Coord = np.empty((PNum,2))
for i in range(PNum):
	Knots[i][0] = r[i/(PNum/len(r))] * np.cos(phi[i%M])
	Knots[i][1] = r[i/(PNum/len(r))] * np.sin(phi[i%M])
	Coord[i][0] = r[i/(PNum/len(r))]
	Coord[i][1] = phi[i%M]


Weight = np.zeros( PNum )
for j in range(len(r)):
	if j == 0:
		Weight[j*M:(j+1)*M] = (r[0]**2 - ( (r[0]+ r[1]  )/2.)**2) * np.pi/M
	
	elif j == len(r)-1:
		Weight[j*M:(j+1)*M] = ((r[-1] +r[-2])/2 ) **2 * np.pi/M
	else:
		Weight[j*M:(j+1)*M] = (( (r[j]+ r[j-1]  )/2.)**2 - ( (r[j]+ r[j+1]  )/2.)**2 )* np.pi/M


A = np.eye(PNum) - dt * Dcon * L
#~ print len(A)
#~ print np.linalg.matrix_rank(A)
M22 = spla.splu(A)
M_x = lambda x: M22.solve(x)
MPre2 = spla.LinearOperator((len(A), len(A)), M_x)

C = np.zeros(PNum)
S = np.zeros(PNum)
xs,ys = np.mgrid[-CellR:CellR:400j, -CellR:CellR:400j]



for i in range(len(C)):
	C[i] = 1. - 0.2*np.cos( 2*np.pi * Coord[i][0]/CellR)


W0 = np.dot(Weight, C)
print W0
PlotFigure(r, phi, C, np.dot(Dr2D,C), np.dot(D2r2D,C), -1)
for step in range(Steps):
	CNew, info   = spla.gmres(A	,C	, x0=C	, M=MPre2	, tol = 1e-15)
	#~ CNew = PolarRfft(CNew,False)
	if step % SaveStep == 0:
		run = step / SaveStep
		PlotFigure(r, phi, CNew, np.dot(Dr2D,CNew), np.dot(D2r2D,CNew), run)
		print str(run)+' '+str( np.dot(Weight, C) )+' '+str((np.dot(Weight, C)-W0)/W0)
	CNew, C = C, CNew

print str(W0)+' '+str(np.dot(Weight, C))+' '+str(W0- np.dot(Weight, C))
