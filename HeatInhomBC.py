import os
import numpy as np
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla

#First and second derivative with FD on general grid
def FD1(x):
	DFD1 = np.zeros((N1,N1))
	DFD1[0][0] = 1./abs(x[0]-x[1])**2
	DFD1[0][1] = -1./abs(x[0]-x[1])**2
	DFD1[-1][-1] = -1./abs(x[-1]-x[-2])**2
	DFD1[-1][-2] = 1./abs(x[-1]-x[-2])**2
	
	for i in range(1,len(DFD1)-1):
		x1 = abs(x[i-1]-x[i])
		x2 = abs(x[i+1]-x[i])
		DFD1[i][i] = (x2-x1)/(x1 * x2)
		DFD1[i][i+1] = x1/(x2*(x2 + x1))
		DFD1[i][i-1] = x2/(x1*(x1 + x2))
	return DFD1

def FD2(x): 
	DFD2 = np.zeros((N1,N1))
	
	DFD2[0][0] = 1./abs(x[0]-x[1])**2
	DFD2[0][1] = -1./abs(x[0]-x[1])**2
	DFD2[-1][-1] = -1./abs(x[-1]-x[-2])**2
	DFD2[-1][-2] = 1./abs(x[-1]-x[-2])**2
	
	for i in range(1,len(DFD2)-1):
		x1 = abs(x[i-1]-x[i])
		x2 = abs(x[i+1]-x[i])
		DFD2[i][i] = - 2./(x1 * x2)
		DFD2[i][i+1] = 2./(x2*(x2 + x1))
		DFD2[i][i-1] = 2./(x1*(x1 + x2))
	
	return DFD2
	
def cheb(N, x0 = -1., x1 = 1.): #return cheb diff Matrix
	D = np.empty((N+1, N+1))
	D[0,0] = (2. * N**2 + 1. )/6.
	D[N,N] = -(2. * N**2 + 1. )/6.
	
	x = np.empty(N1)
	for i in range(N1):
		x[i] = np.cos((i * np.pi)/N) 
	
	for i in range(1,N):
		D[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,N1):
		for j in range(0,N1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == N :
					ci = 2.
				if j == 0 or j == N :
					cj = 2.
				D[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	
	y = np.empty(N1)
	for i in range(N1):
		y[i] = (x[i]+1)/2 * x1
	
	return (2./(x1-x0) *D, y)


def BuildMatrix(D1, D2):
	Temp = np.eye(N1) - dt * Dcon * D2
	
	#Boundary conditions in system matrix, NF (right) and Dir (left). Inhomogenities can be set in the timeloop
	Temp[0,:] = D1[0]
	Temp[-1,:] = 0.
	Temp[-1,-1] = 1.
	
	return Temp


def PlotFigure(x, C, run):
	f, ax3 = plt.subplots(2, 1)
	ax3[0].plot(x,C)
	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','Heat_'+str(run)+'.png'),bbox_inches='tight')
	plt.close(f)

L = 2.
dt = 0.1
Dcon = 1.
EndTime = 2.
SaveTime = 0.1

Prec = 2 #0 = FD, 1 = ILU, 2 = FD-ILU

Step = int(EndTime/dt)+1
SaveStep = int(SaveTime/dt)

N = 21
N1 = N+1
(CD, x) = cheb(N, 0, L)
CD2 = np.dot(CD, CD)

path = 'HeatInhomBC'
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))

A = BuildMatrix(CD, CD2)
start = time.clock()

print 'Condition number :'+str( np.linalg.cond(A) )
if Prec  == 0:
	def applyPrec(v):   # Define action of preconditioner
		return spla.spsolve(-csr_matrix(AFD),v)
	AFD = BuildMatrix(FD1(x), FD2(x))
	M = spla.LinearOperator((len(A),len(A)),matvec=applyPrec, dtype=float)
elif Prec == 1:
	M22 = spla.spilu(A)
	M_x = lambda x: M22.solve(x)
	M = spla.LinearOperator((len(A), len(A)), M_x)
elif Prec == 2:
	AFD = BuildMatrix(FD1(x),FD2(x))
	M22 = spla.spilu(AFD)
	M_x = lambda x: M22.solve(x)
	M = spla.LinearOperator((len(A), len(A)), M_x)
else:
	print 'Choose a valid preconditioner'
	exit(1)

C = 0.5+0.5*np.tanh(3*(0.2 - (x-L/2.)**2)/0.5) #initial

PlotFigure(x, C, -1)

for t in range(Step):
	#BC
	C[0] = -0.1 # Flux from the right
	C[-1] = 0.75 #Dirichlet value on the left
	
	CNew, info = spla.lgmres(A,C, M=M, tol=1e-10)
	
	if t % SaveStep == 0 or t == Step-1:
		run = t / SaveStep
		print( 'Run: '+str(run)+' Time: '+str(time.clock() - start)+'s' )
		PlotFigure(x, CNew, run)
		
	CNew, C = C, CNew
