import os
import numpy as np
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla

def FD1Per(m):
	DFD1 = np.zeros((m,m))
	
	h = 2*np.pi/m
	
	#periodic Boundary
	DFD1[0][1] = 1./2.
	DFD1[0][-1] = -1./2.
	DFD1[-1][-2] = -1./2.
	DFD1[-1][0] = 1./2.
	for i in range(1,len(DFD1)-1):
		DFD1[i][i+1] = 1./2.
		DFD1[i][i-1] = -1./2.
	
	return 1./h*DFD1
	
def FD2Per(m):
	DFD2 = np.zeros((m,m))
	
	h = 2*np.pi/m
	
	#periodic Boundary
	DFD2[0][0] = - 2.
	DFD2[0][1] = 1.
	DFD2[0][-1] = 1.
	DFD2[-1][-1] = - 2.
	DFD2[-1][-2] = 1.
	DFD2[-1][0] = 1.
	for i in range(1,len(DFD2)-1):
		DFD2[i][i] = - 2.
		DFD2[i][i+1] = 1.
		DFD2[i][i-1] = 1.
	
	return DFD2*1./h**2
	
def Spec(n):#return spectral diff Matrix (periodic)
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	i = np.array( range(1,n) )
	column = np.hstack(( [0.], .5*(-1)**i/np.tan(i*h/2.) ))
	row = np.hstack(([0.], column[n-1:0:-1] ))
	D = linalg.toeplitz(column,row)
	return (D, x)
	
def BuildMatrix(D2):
	return np.eye(N) - dt * Dcon * D2
	
	

def PlotFigure(x, C, run):
	f, ax3 = plt.subplots(3, 1)
	ax3[0].plot(x,C)
	ax3[1].plot(x,np.dot(D,C))
	ax3[2].plot(x,np.dot(Dfd,C))
	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','Heat_'+str(run)+'.png'),bbox_inches='tight')
	plt.close(f)

N = 500
LengthX = 2*np.pi
dt = 0.01
Dcon = 1.
EndTime = 10
SaveTime = 1

Prec = 3 #0 = no, 1 = FD, 2 = ILU, 3 = FD-ILU

Step = int(EndTime/dt)+1
SaveStep = int(SaveTime/dt)

(D, x) = Spec(N)
Dfd = FD1Per(N)
CD2 = np.dot(D, D)

path = 'HeatPerPrec'
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))


	
A = BuildMatrix(CD2)
AFD = BuildMatrix(FD2Per(N))

start = time.clock()

print 'Cond :'+str( np.linalg.cond(A) )
if Prec  == 1:
	def applyPrec(v):   # Define action of preconditioner
		return spla.spsolve(-csr_matrix(AFD),v)
	M = spla.LinearOperator((len(A),len(A)),matvec=applyPrec, dtype=float)
elif Prec == 2:
	M22 = spla.spilu(A)
	M_x = lambda x: M22.solve(x)
	M = spla.LinearOperator((len(A), len(A)), M_x)
elif Prec == 3:
	M22 = spla.spilu(AFD)
	M_x = lambda x: M22.solve(x)
	M = spla.LinearOperator((len(A), len(A)), M_x)

Chem = np.zeros(2*PNum)
xs,ys = np.mgrid[-CellR:CellR:250j, -CellR:CellR:250j]

for i in range(PNum):
	rad = (Knots[i,0])**2 + (Knots[i,1]+40.)**2 
	Chem[i] 	= 0.5+0.5*np.tanh(3*(3000. - (Coord[i,0])**2)/20000.);
	Chem[i+PNum] = 0.3+0.3*np.tanh(3*(3000. - rad)/20000.);

PlotFigure(x, C, -1)

def applyChem(vv):
	(U, W) = (vv[:PNum], vv[PNum:])
	
	uu = np.dot(A,U) - dt*( U*(U-a)*(1.-U)  - W + I )
	ww = W - dt*eps*(U - g*W)
	return np.hstack( (uu, ww))

def applyChemMat(v):
	aa = np.zeros_like(v)
	for i in range(len(aa)):
		aa[:,i] = applyChem(v[:,i])
	return aa

I = 0.0
a = 0.002
g = 0.5

eps = 0.0024804
tol= 1e-8

ChemOP = spla.LinearOperator((2*PNum,2*PNum),matvec=applyChem,matmat=applyChemMat,dtype=float)
ChemOPMat = ChemOP.matmat(np.eye(2*PNum))
ILUC = spla.splu(ChemOPMat)
def applyCILUPrec(v):        # Define action of preconditioner
	return ILUC.solve(v)
PC = spla.LinearOperator((2*PNum,2*PNum),matvec=applyCILUPrec,dtype=float)

for t in range(Step):
	CNew, info = spla.gmres(ChemOP,C, M=PC, tol=1e-10)
	
	if t % SaveStep == 0 or t == Step-1:
		run = t / SaveStep
		print( str(run)+'_'+str(time.clock() - start) )
		PlotFigure(x, CNew, run)
		
	CNew, C = C, CNew
