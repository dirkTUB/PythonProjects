import os
import numpy as np
import scipy as sp
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla
from scipy.interpolate import griddata
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pyamg as pa
from scipy.fftpack import idct, dct

def Spec(n):
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	i = np.array( range(1,n) )
	column = np.hstack(( [0.], .5*(-1)**i/np.tan(i*h/2.) ))
	row = np.hstack(([0.], column[n-1:0:-1] ))
	D = linalg.toeplitz(column,row)
	return (D, x)

def cheb(n): #return cheb diff Matrix
	DTemp = np.empty((n+1, n+1))
	DTemp[0,0] = (2. * n**2 + 1. )/6.
	DTemp[n,n] = -(2. * n**2 + 1. )/6.
	
	x = np.empty(n+1)
	for i in range(n+1):
		x[i] = np.cos((i * np.pi)/n) 
	
	for i in range(1,n):
		DTemp[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,n+1):
		for j in range(0,n+1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == n :
					ci = 2.
				if j == 0 or j == n :
					cj = 2.
				DTemp[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	return (DTemp, x)

def MultiGrid(ATemp):
	B = np.ones((ATemp.shape[0],1))
	smooth=('jacobi', {'omega': 4.0/3.0,'degree':2})
	SA_build_args={'max_levels':10, 'max_coarse':5}
	SA_solve_args={'cycle':'V', 'maxiter':50, 'tol':1e-15}
	strength=[('evolution')]
	presmoother=('block_gauss_seidel', {'sweep': 'symmetric'})
	postsmoother=('block_gauss_seidel', {'sweep': 'symmetric'})
	improve_candidates =[('block_gauss_seidel', {'sweep': 'symmetric', 'iterations': 4}), None]
	MG = pa.smoothed_aggregation_solver(csr_matrix(ATemp),B,  BH = B.copy(), keep=True, smooth=smooth, strength=strength, improve_candidates=improve_candidates, presmoother=presmoother, postsmoother=postsmoother, **SA_build_args)
	return (MG, SA_solve_args)


def BuildRMatrix():
	(CD, r) = cheb(N)
	r = r*CellR
	CD = CD*1./CellR
	CDN = np.copy(CD)
	CDN[0] = 0
	CDN[-1] = 0
	CD2 = np.dot(CD, CD)
	CD2N = np.dot(CD, CDN)
	if Bound ==0:
		r = r[1:N2+1]
		
		D1 = CD2[1:N2+1,1:N2+1]
		D2 = np.fliplr(CD2[1:N2+1,N2+1:-1])
		E1 = CD[1:N2+1,1:N2+1]
		E2 = np.fliplr(CD[1:N2+1,N2+1:-1])
	elif Bound == 1:
		r = r[:N2+1]
		
		D1 = CD2N[:N2+1,:N2+1]
		D2 = np.fliplr(CD2N[:N2+1,N2+1:])
		E1 = CD[:N2+1,:N2+1]
		E2 = np.fliplr(CD[:N2+1,N2+1:])
	elif Bound == 2:
		r = r[:N2+1]
		
		D1 = CD2[:N2+1,:N2+1]
		#~ D1[0] = 0
		#~ D1[:,0] = 0
		E1 = CD[:N2+1,:N2+1]
		#~ E1[0] = 0
		#~ E1[:,0] = 0
		
		D2 = np.fliplr(CD2[:N2+1,N2+1:])
		#~ D2[0] = 0
		#~ D2[:,0] = 0
		
		E2 = np.fliplr(CD[:N2+1,N2+1:])
		#~ E2[0] = 0
		#~ E2[:,0] = 0

	
	return (D1, D2, E1, E2, r)


def PlotFigure(rad, the, C, Dphi, D2, run):
	Cres = griddata(Knots, C, (xs, ys), method='linear')
	Dphires = griddata(Knots, Dphi, (xs, ys), method='linear')
	D2phires = griddata(Knots, D2, (xs, ys), method='linear')
	fig, (ax, ax2, ax3) = plt.subplots(1, 3, figsize = (3*5,5))
	ax.set_title('State')
	im = ax.matshow(Cres.T, extent=[-CellR,CellR,-CellR,CellR])
	divider = make_axes_locatable(ax)
	cax = divider.append_axes("right", size="8%", pad=0.05)
	cbar = plt.colorbar(im, cax=cax)
	
	ax2.set_title('dr')
	im2 = ax2.matshow(Dphires.T, extent=[-CellR,CellR,-CellR,CellR])
	divider2 = make_axes_locatable(ax2)
	cax2 = divider2.append_axes("right", size="8%", pad=0.05)
	cbar2 = plt.colorbar(im, cax=cax2)
	
	ax3.set_title('d2r')
	im3 = ax3.matshow(D2phires.T, extent=[-CellR,CellR,-CellR,CellR])
	divider3 = make_axes_locatable(ax3)
	cax3 = divider3.append_axes("right", size="8%", pad=0.05)
	cbar3 = plt.colorbar(im, cax=cax3)

	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','State{0:04d}.png'.format(run)),bbox_inches='tight')
	plt.close(plt.gcf())

CellR = 75
InitialR = 2
dt = 0.01
Dcon = 200
EndTime = 5.
SaveTime = 0.5
Bound = 1 #0 = Dir, 1 = no Flux, 2: r no-flux, l = Dir0

path = 'HeatPolar'
Steps = int(EndTime/dt)+1
SaveStep = int(SaveTime/dt)
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))
if not os.path.exists(os.path.join(path,'FFT')): #create Folders for Output State
	os.makedirs(os.path.join(path,'FFT'))

#radius
N = 51 #odd
N2 = (N-1)/2

(D1, D2, E1, E2, r) = BuildRMatrix()
CellR = r.max() # we might have thrown away the max radius (dirichlet)
R = np.diag(1./r)


#phi
M = 20 #even
M2 = M/2
(Dphi,phi) = Spec(M)
D2phi = np.dot(Dphi,Dphi)

#construct Matrices

Z = np.zeros((M2,M2))
I = np.eye(M2)
AA = np.vstack((np.hstack((Z,I)), np.hstack((I,Z))) )
L = np.kron(D1+np.dot(R,E1),np.eye(M)) + np.kron(D2+np.dot(R,E2),AA) + np.kron(np.dot(R,R),D2phi)
#~ L = np.kron(D1+np.dot(R,E1),np.eye(M)) + np.kron(D2+np.dot(R,E2),AA)# + np.kron(np.dot(R,R),D2phi)

Dr2d = (np.kron(E1,np.eye(M))+ np.kron(E2,AA))
D2r2d = (np.kron(D1,np.eye(M))+ np.kron(D2,AA))
AdvRad = -(np.kron(E1,np.eye(M))+ np.kron(E2,AA))*1.*dt
AdvPhi = np.kron(np.eye(len(r)),Dphi )*dt*0.1

PNum = len(L)
Knots = np.empty((PNum,2))
Coord = np.empty((PNum,2))
for i in range(PNum):
	Knots[i][0] = r[i/(PNum/len(r))] * np.cos(phi[i%M])
	Knots[i][1] = r[i/(PNum/len(r))] * np.sin(phi[i%M])
	Coord[i][0] = r[i/(PNum/len(r))]
	Coord[i][1] = phi[i%M]
	
Weight = np.zeros( PNum )
for j in range(len(r)):
	if j == 0:
		Weight[j*M:(j+1)*M] = (r[0]**2 - ( (r[0]+ r[1]  )/2.)**2) * np.pi/M
	
	elif j == len(r)-1:
		Weight[j*M:(j+1)*M] = ((r[-1] +r[-2])/2 ) **2 * np.pi/M
	else:
		Weight[j*M:(j+1)*M] = (( (r[j]+ r[j-1]  )/2.)**2 - ( (r[j]+ r[j+1]  )/2.)**2 )* np.pi/M

A = np.eye(PNum) - dt * Dcon * L + AdvPhi + AdvRad
print len(A)
#~ print np.linalg.matrix_rank(A)
M22 = spla.splu(A)
M_x = lambda x: M22.solve(x)
MPre2 = spla.LinearOperator((len(A), len(A)), M_x)

C = np.zeros(PNum)
S = np.zeros(PNum)
xs,ys = np.mgrid[-CellR:CellR:500j, -CellR:CellR:500j]

for i in range(len(C)):
	rad = (Knots[i][0]+0.75)**2 + (Knots[i][1]+0.75)**2 
	#~ C[i] = 0.5+0.5*np.tanh(3*(25-Coord[i][0])/5.5)
	C[i] = 1. + 0.5*np.sin( 2*np.pi * Knots[i][0]/CellR)
	
	#~ if Coord[i][0] < 1:
		#~ S[i] = 0.1
		#~ C[i] += S[i]
		
	#~ if Coord[i][0] == CellR:
		#~ C[i] == 0.
		#~ S[i] == 0.
		
def PolarRfft(c,plot):
	if plot:
		fig, ax = plt.subplots(1, 3)
	
	cnew = np.empty(PNum)
	
	for i in range(M2):
		rr1 = c[i::M]
		rr2 = c[i+M2::M]
		#~ rr = np.hstack( (rr1,rr2[::-1], rr2[1:], rr1[:-1:-1] ))
		rr = np.hstack( (rr1,rr2[::-1] ))
		r2 = np.hstack( (rr,rr[-1:1:-1] ))

		#~ ax.plot(r2)
		#~ rrfft = np.fft.fft(r2)

		
		rrfft=dct(r2, norm='ortho')
		freq = np.fft.fftfreq(rrfft.shape[-1])
		if plot:
			ax[0].plot(rr)
			ax[1].plot(rrfft)
			ax[2].plot(freq, rrfft.real, freq, rrfft.imag)


		rrfft[-6:] = 0
		
		rr = idct(rrfft, norm='ortho')[:len(rr)]
		rrs = rr[len(rr1):]
		cnew[i::M] = rr[:len(rr1)]
		cnew[i+M2::M] = rrs[::-1]
	if plot:
		plt.tight_layout()
		plt.savefig(os.path.join(path,'FFT','FFT_'+str(run)+'.png'),bbox_inches='tight')
		plt.close(plt.gcf())
	#~ exit(1)
	return cnew

W0 = np.dot(Weight, C)
PlotFigure(r, phi, C, np.dot(Dr2d,C), np.dot(D2r2d,C), -1)
for step in range(Steps):
	CNew, info   = spla.lgmres(A	,C	, x0=C	, M=MPre2	, tol = 1e-15)
	#~ CNew = PolarRfft(CNew,False)
	if step % SaveStep == 0:
		run = step / SaveStep
		CNewa = PolarRfft(CNew,True)
		PlotFigure(r, phi, CNew, np.dot(Dr2d,CNew), np.dot(D2r2d,CNew), run)
		print str(run)+' '+str( np.dot(Weight, C) )+' '+str((np.dot(Weight, C)-W0)/W0)
	CNew, C = C, CNew

print str(W0)+' '+str(np.dot(Weight, C))+' '+str(W0- np.dot(Weight, C))
