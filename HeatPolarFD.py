import os
import numpy as np
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla
from scipy.interpolate import griddata
from mpl_toolkits.axes_grid1 import make_axes_locatable

def FD1(x):
	DFD1 = np.zeros((len(x),len(x)))
	DFD1[0][0] = 1./abs(x[0]-x[1])
	DFD1[0][1] = -1./abs(x[0]-x[1])
	
	DFD1[-1][-1] = -1./abs(x[-1]-x[-2])
	DFD1[-1][-2] = 1./abs(x[-1]-x[-2])
	
	for i in range(1,len(DFD1)-1):
		x1 = abs(x[i-1]-x[i])
		x2 = abs(x[i+1]-x[i])
		DFD1[i][i] = (x2-x1)/(x1 * x2)
		DFD1[i][i+1] = x1/(x2*(x2 + x1))
		DFD1[i][i-1] = -x2/(x1*(x1 + x2))
	return -1*DFD1

def FD2(x):
	DFD2 = np.zeros((len(x),len(x)))
	
	DFD2[0][0] = 1./abs(x[0]-x[1])**2
	DFD2[0][1] = -1./abs(x[0]-x[1])**2
	
	DFD2[-1][-1] = -1./abs(x[-1]-x[-2])**2
	DFD2[-1][-2] = 1./abs(x[-1]-x[-2])**2
	
	for i in range(1,len(DFD2)-1):
		x1 = abs(x[i-1]-x[i])
		x2 = abs(x[i+1]-x[i])
		DFD2[i][i] = - 2./(x1 * x2)
		DFD2[i][i+1] = 2./(x2*(x2 + x1))
		DFD2[i][i-1] = 2./(x1*(x1 + x2))
	
	return DFD2

def FD2Per(m):
	DFD2 = np.zeros((m,m))
	
	h = 2*np.pi/m
	
	#periodic Boundary
	DFD2[0][0] = - 2.
	DFD2[0][1] = 1.
	DFD2[0][-1] = 1.
	DFD2[-1][-1] = - 2.
	DFD2[-1][-2] = 1.
	DFD2[-1][0] = 1.
	for i in range(1,len(DFD2)-1):
		DFD2[i][i] = - 2.
		DFD2[i][i+1] = 1.
		DFD2[i][i-1] = 1.
	
	return DFD2*1./h**2
	
def FD1Per(m):
	DFD1 = np.zeros((m,m))
	
	h = 2*np.pi/m
	
	#periodic Boundary
	DFD1[0][1] = 1./2.
	DFD1[0][-1] = -1./2.
	DFD1[-1][-2] = -1./2.
	DFD1[-1][0] = 1./2.
	for i in range(1,len(DFD1)-1):
		DFD1[i][i+1] = 1./2.
		DFD1[i][i-1] = -1./2.
	
	return 1./h*DFD1

def Spec(n):
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	i = np.array( range(1,n) )
	column = np.hstack(( [0.], .5*(-1)**i/np.tan(i*h/2.) ))
	row = np.hstack(([0.], column[n-1:0:-1] ))
	D = linalg.toeplitz(column,row)
	return (D, x)

def cheb(n): #return cheb diff Matrix
	DTemp = np.empty((n+1, n+1))
	DTemp[0,0] = (2. * n**2 + 1. )/6.
	DTemp[n,n] = -(2. * n**2 + 1. )/6.
	
	x = np.empty(n+1)
	for i in range(n+1):
		x[i] = np.cos((i * np.pi)/n) 
	
	for i in range(1,n):
		DTemp[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,n+1):
		for j in range(0,n+1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == n :
					ci = 2.
				if j == 0 or j == n :
					cj = 2.
				DTemp[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	return (DTemp, x)

def BuildRMatrix():
	(CD, r) = cheb(N)
	r = r*CellR
	CD = CD*1./CellR
	CDN = np.copy(CD)
	CDN[0] = 0
	CDN[-1] = 0
	CD2 = np.dot(CD, CD)
	CD2N = np.dot(CD, CDN)
	if Bound ==0:
		r = r[1:N2+1]
		
		D1 = CD2[1:N2+1,1:N2+1]
		D2 = np.fliplr(CD2[1:N2+1,N2+1:-1])
		E1 = CD[1:N2+1,1:N2+1]
		E2 = np.fliplr(CD[1:N2+1,N2+1:-1])
	elif Bound == 1:
		r = r[:N2+1]
		
		D1 = CD2N[:N2+1,:N2+1]
		D2 = np.fliplr(CD2N[:N2+1,N2+1:])
		E1 = CDN[:N2+1,:N2+1]
		E2 = np.fliplr(CDN[:N2+1,N2+1:])
	elif Bound == 2:
		r = r[:N2+1]
		
		D1 = CD2[:N2+1,:N2+1]
		D1[0] = 0
		D1[:,0] = 0
		E1 = CD[:N2+1,:N2+1]
		E1[0] = 0
		E1[:,0] = 0
		
		D2 = np.fliplr(CD2[:N2+1,N2+1:])
		D2[0] = 0
		D2[:,0] = 0
		
		E2 = np.fliplr(CD[:N2+1,N2+1:])
		E2[0] = 0
		E2[:,0] = 0

	
	return (D1, D2, E1, E2, r)


def PlotFigure(rad, the, C, Dphi, run, method):
	Cres = griddata(Knots, C, (xs, ys), method='linear')
	Dphires = griddata(Knots, Dphi, (xs, ys), method='linear')
	fig, (ax, ax2) = plt.subplots(1, 2)
	im = ax.matshow(Cres.T, extent=[-CellR,CellR,-CellR,CellR])
	divider = make_axes_locatable(ax)
	cax = divider.append_axes("right", size="8%", pad=0.05)
	cbar = plt.colorbar(im, cax=cax)
	
	im2 = ax2.matshow(Dphires.T, extent=[-CellR,CellR,-CellR,CellR])
	divider2 = make_axes_locatable(ax2)
	cax2 = divider2.append_axes("right", size="8%", pad=0.05)
	cbar2 = plt.colorbar(im, cax=cax2)

	plt.tight_layout()
	plt.savefig(os.path.join(path,'State{}'.format(method),'Heat{}_'.format(method)+str(run)+'.png'),bbox_inches='tight')
	plt.close(plt.gcf())

CellR = 75
InitialR = 2
dt = 0.01
Dcon = 200.
EndTime = 25
SaveTime = 2.5
Bound = 1 #0 = Dir, 1 = no Flux, 2: r no-flux, l = Dir0

path = 'HeatPolarFD'
Steps = int(EndTime/dt)+1
SaveStep = int(SaveTime/dt)
if not os.path.exists(os.path.join(path,'StateFD')): #create Folders for Output State
	os.makedirs(os.path.join(path,'StateFD'))
if not os.path.exists(os.path.join(path,'StateCH')): #create Folders for Output State
	os.makedirs(os.path.join(path,'StateCH'))

#radius
N = 61 #odd
N2 = (N-1)/2

(D1, D2, E1, E2, r) = BuildRMatrix()
CellR = r.max() # we might have thrown away the max radius (dirichlet)
R = np.diag(1./r)

#phi
M = 14 #even
M2 = M/2
(Dphi,phi) = Spec(M)
D2phi = np.dot(Dphi,Dphi)

#construct Matrices

Z = np.zeros((M2,M2))
I = np.eye(M2)
AA = np.vstack((np.hstack((Z,I)), np.hstack((I,Z))) )
L = np.kron(D1+np.dot(R,E1),np.eye(M)) + np.kron(D2+np.dot(R,E2),AA) + np.kron(np.dot(R,R),D2phi)
#~ L = np.kron(np.dot(R,R),D2phi)
LFD = np.kron(FD2(r) ,np.eye(M)) + np.kron(np.dot(R,FD1(r) ), np.eye(M)) + np.kron(np.dot(R,R),FD2Per(M))
#~ LFD = np.kron(np.dot(R,R),FD2Per(M))

AdvRad = -(np.kron(E1,np.eye(M))+ np.kron(E2,AA))*0.4*dt
AdvPhi = np.kron(R,Dphi )*dt*5.
AdvPhiFD = np.kron(R,FD1Per(M) )*dt*5.

PNum = len(L)
Knots = np.empty((PNum,2))
Coord = np.empty((PNum,2))
for i in range(PNum):
	Knots[i][0] = r[i/(PNum/len(r))] * np.cos(phi[i%M])
	Knots[i][1] = r[i/(PNum/len(r))] * np.sin(phi[i%M])
	Coord[i][0] = r[i/(PNum/len(r))]
	Coord[i][1] = phi[i%M]

A = np.eye(PNum) - dt * Dcon * L
AFD = np.eye(PNum) - dt * Dcon * LFD
#~ AFD = csr_matrix(AFD)

f, ax = plt.subplots(2, 2,figsize=(2*5,2*5))

ax[0][0].set_title(r'Cheb', fontsize=25, y=1.02)
ax[0][1].set_title(r'FD', fontsize=25, y=1.02)

w, v = linalg.eig(A )
ax[0][0].plot(w.real)
ax[1][0].plot(w.imag)
w, v = linalg.eig(AFD )
ax[0][1].plot(w.real)
ax[1][1].plot(w.imag)
plt.tight_layout()
plt.savefig(os.path.join(path,'EW.png'),bbox_inches='tight')
plt.close(f)


Prec = 2
if Prec  == 1:
	def applyPrec(v):   # Define action of preconditioner
		return spla.spsolve(-csr_matrix(AFD),v)
	M2 = spla.LinearOperator((len(A),len(A)),matvec=applyPrec)
elif Prec == 2:
	M22 = spla.splu(A)
	M_x = lambda x: M22.solve(x)
	M2 = spla.LinearOperator((len(A), len(A)), M_x)
	MAFD22 = spla.splu(AFD)
	MAFD_x = lambda x: MAFD22.solve(x)
	MAFD2 = spla.LinearOperator((len(A), len(A)), MAFD_x)
elif Prec == 3:
	M22 = spla.splu(AFD)
	M_x = lambda x: M22.solve(x)
	M2 = spla.LinearOperator((len(A), len(A)), M_x)

C = np.zeros(PNum)
S = np.zeros(PNum)
xs,ys = np.mgrid[-CellR:CellR:500j, -CellR:CellR:500j]

for i in range(len(C)):
	C[i] = 0.5+0.5*np.tanh(3*(30-Coord[i][0])/15.)
	#~ C[i] = 1+np.sin(Coord[i][1])
	#~ C[i] = 1+np.sin(Coord[i][0]*0.01)
	
	#~ if Coord[i][0] < 1:
		#~ S[i] = 0.1
		#~ C[i] += S[i]
		
	#~ if Coord[i][0] == CellR:
		#~ C[i] == 0.
		#~ S[i] == 0.

Weight = np.zeros( PNum )
for j in range(len(r)):
	if j == 0:
		Weight[j*M:(j+1)*M] = (r[0]**2 - ( (r[0]+ r[1]  )/2.)**2) * np.pi/M
	
	elif j == len(r)-1:
		Weight[j*M:(j+1)*M] = ((r[-1] +r[-2])/2 ) **2 * np.pi/M
	else:
		Weight[j*M:(j+1)*M] = (( (r[j]+ r[j-1]  )/2.)**2 - ( (r[j]+ r[j+1]  )/2.)**2 )* np.pi/M
W0 = np.dot(Weight,C)
CFD = np.copy(C)
start = time.clock()
PlotFigure(r, phi, C, np.dot(L,C), -1, 'CH')
PlotFigure(r, phi, CFD, np.dot(LFD,CFD), -1, 'FD')
for step in range(Steps):
	if Prec == 0:
		CNew, info   = spla.lgmres(A		,C	, x0=C	, tol = 1e-15)
	else:
		CNew, info   = spla.lgmres(A	,C	, x0=C	, M=M2	, tol = 1e-15)
		#~ CNewFD, info = spla.gmres(AFD	,CFD, x0=CFD, M=MAFD2, tol = 1e-15)
	
	if step % SaveStep == 0:
		run = step / SaveStep
		
		PlotFigure(r, phi, CNew, np.dot(L,CNew), run, 'CH')
		#~ PlotFigure(r, phi, CNewFD, np.dot(LFD,CNewFD), run, 'FD')
		print str(run)+' '+str(time.clock() - start)+' '+str(np.dot(Weight,CNew))
		#~ print abs((CNew-CNewFD)[:] ).sum()
	CNew, C = C, CNew
	#~ CNewFD, CFD = CFD, CNewFD
print W0- np.dot(Weight, C)
