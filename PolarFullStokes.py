import os
import numpy as np
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla
import time

def Spec(n):#return spectral diff Matrix (periodic)
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	i = np.array( range(1,n) )
	column = np.hstack(( [0.], .5*(-1)**i/np.tan(i*h/2.) ))
	row = np.hstack(([0.], column[n-1:0:-1] ))
	D = linalg.toeplitz(column,row)
	return (D, x)
	
def FD1Per(m):
	DFD1 = np.zeros((m,m))
	
	h = 2*np.pi/m
	scale = 1./h
	
	#periodic Boundary
	DFD1[0][1] = scale/2.
	DFD1[0][-1] = -scale/2.
	DFD1[-1][-2] = -scale/2.
	DFD1[-1][0] = scale/2.
	for i in range(1,len(DFD1)-1):
		DFD1[i][i+1] = scale/2.
		DFD1[i][i-1] = -scale/2.
	
	return DFD1

def FD2Per(m):
	DFD2 = np.zeros((m,m))
	
	h = 2*np.pi/m
	
	#periodic Boundary
	DFD2[0][0] = - 2.
	DFD2[0][1] = 1.
	DFD2[0][-1] = 1.
	DFD2[-1][-1] = - 2.
	DFD2[-1][-2] = 1.
	DFD2[-1][0] = 1.
	for i in range(1,len(DFD2)-1):
		DFD2[i][i] = - 2.
		DFD2[i][i+1] = 1.
		DFD2[i][i-1] = 1.
	
	return DFD2*1./h**2

def cheb(N, x0 = 0.1, x1 = 1):
	D = np.empty((N+1, N+1))
	D[0,0] = (2. * N**2 + 1. )/6.
	D[N,N] = -(2. * N**2 + 1. )/6.
	
	x = np.empty(N+1)
	for i in range(N+1):
		x[i] = np.cos((i * np.pi)/N) 
	
	for i in range(1,N):
		D[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,N+1):
		for j in range(0,N+1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == N :
					ci = 2.
				if j == 0 or j == N :
					cj = 2.
				D[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	
	y = np.empty(N+1)
	for i in range(N+1):
		y[i] = (x[i]+1 )/2 * x1 + x0
	
	return (2./(x1-x0) *D, y)

def FD1(x):
	DFD1 = np.zeros((len(x),len(x)))
	DFD1[0][0] = -1./abs(x[0]-x[1])
	DFD1[0][1] = 1./abs(x[0]-x[1])
	DFD1[-1][-1] = 1./abs(x[-1]-x[-2])
	DFD1[-1][-2] = -1./abs(x[-1]-x[-2])
	
	for i in range(1,len(DFD1)-1):
		x1 = abs(x[i-1]-x[i])
		x2 = abs(x[i+1]-x[i])
		DFD1[i][i] = (x2-x1)/(x1 * x2)
		DFD1[i][i+1] = x1/(x2*(x2 + x1))
		DFD1[i][i-1] = x2/(x1*(x1 + x2))
	return DFD1

def FD2(x):
	DFD2 = np.zeros((len(x),len(x)))
	
	DFD2[0][0] = -1./abs(x[0]-x[1])
	DFD2[0][1] = 1./abs(x[0]-x[1])
	DFD2[-1][-1] = -1./abs(x[-1]-x[-2])
	DFD2[-1][-2] = 1./abs(x[-1]-x[-2])
	
	for i in range(1,len(DFD2)-1):
		x1 = abs(x[i-1]-x[i])
		x2 = abs(x[i+1]-x[i])
		DFD2[i][i] = - 2./(x1 * x2)
		DFD2[i][i+1] = 2./(x2*(x2 + x1))
		DFD2[i][i-1] = 2./(x1*(x1 + x2))
	
	return -DFD2

def PlotFigure():
	brC = griddata(Knots, b[u_r*PNum:(u_r+1)*PNum], (xs, ys), method='nearest')
	bpC = griddata(Knots, b[u_p*PNum:(u_p+1)*PNum], (xs, ys), method='nearest')
	UrC = griddata(Knots, w[u_r*PNum:(u_r+1)*PNum], (xs, ys), method='nearest')
	UpC = griddata(Knots, w[u_p*PNum:(u_p+1)*PNum], (xs, ys), method='nearest')
	PresC = griddata(Knots, w[pre*PNum:(pre+1)*PNum], (xs, ys), method='nearest')
	fig, ax = plt.subplots(1, 3,figsize=(3*5,5))
	DatList= [UrC, UpC, PresC]
	ax[0].set_title('u_r')
	ax[1].set_title('u_p')
	ax[2].set_title('pressure')
	for i in range(len(DatList)):
		im = ax[i].imshow(DatList[i].T, extent=[-CellR,CellR,-CellR,CellR])
		divider2 = make_axes_locatable(ax[i])
		cax2 = divider2.append_axes("right", size="8%", pad=0.05)
		cbar2 = plt.colorbar(im, cax=cax2)
	
	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','State{}.png'.format(run)))
	plt.close(fig)
	
	
	XvelR = w[u_r:(u_r+1)*PNum]*np.cos(Coord[:,1]) -w[u_p*PNum:(u_p+1)*PNum]*np.sin(Coord[:,1])
	YvelR = w[u_r:(u_r+1)*PNum]*np.sin(Coord[:,1]) + w[u_p*PNum:(u_p+1)*PNum]*np.cos(Coord[:,1])

	XvelP = -w[u_p*PNum:(u_p+1)*PNum]*np.sin(Coord[:,1])
	YvelP = w[u_p*PNum:(u_p+1)*PNum]*np.cos(Coord[:,1])
	
	fig, (ax1, ax2) = plt.subplots(1, 2,figsize=(2*5,5))
	ax1.set_title('rhs phi, vel in phi direction')
	im1 = ax1.matshow(bpC.T, extent=[-CellR,CellR,-CellR,CellR],cmap='cool')
	divider1 = make_axes_locatable(ax1)
	cax1 = divider1.append_axes("right", size="8%", pad=0.05)
	cbar1 = plt.colorbar(im1, cax=cax1)
	KnotMod = 1
	v = ax1.quiver([x[0] for x in Knots[::KnotMod]],[x[1] for x in Knots[::KnotMod]],XvelP[::KnotMod],YvelP[::KnotMod])
	
	ax2.set_title('rhs r, full velocity')
	im2 = ax2.matshow(brC.T, extent=[-CellR,CellR,-CellR,CellR],cmap='cool')
	divider2 = make_axes_locatable(ax2)
	cax2 = divider2.append_axes("right", size="8%", pad=0.05)
	cbar2 = plt.colorbar(im2, cax=cax2)
	v = ax2.quiver([x[0] for x in Knots[::KnotMod]],[x[1] for x in Knots[::KnotMod]],XvelR[::KnotMod],YvelR[::KnotMod])

	plt.tight_layout()
	plt.savefig(os.path.join(path,'Vel','Vel{}.png'.format(run)))
	plt.close(fig)
	
	

CellR = 75.
Vis = 0.1
dt=0.01
Endstep=1000
SaveStep = Endstep/10

path = 'PolarFullStokes'
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))
if not os.path.exists(os.path.join(path,'Vel')): #create Folders for Output State
	os.makedirs(os.path.join(path,'Vel'))
if not os.path.exists(os.path.join(path,'1D')): #create Folders for Output State
	os.makedirs(os.path.join(path,'1D'))

#radius
Nr = 21 #odd
N1r = Nr+1 #odd

(CD,r) = cheb(Nr, 0.1, CellR)
print r[0]
CDN = np.copy(CD)
CDN[0,:] = 0
CD2 = np.dot(CD, CD)
CD2N = np.dot(CD, CDN)
R = np.diag(1./r)

#theta
M = 10 #even
M2 = M/2
(Dphi,phi) = Spec(M)
D2phi = np.dot(Dphi,Dphi)

PNum= M*N1r #Num of points (for each var)
Knots = np.empty((PNum,2))
Coord = np.empty((PNum,2))
for i in range(PNum):
	Knots[i][0] = r[i/(PNum/len(r))] * np.cos(phi[i%M])
	Knots[i][1] = r[i/(PNum/len(r))] * np.sin(phi[i%M])
	Coord[i][0] = r[i/(PNum/len(r))]
	Coord[i][1] = phi[i%M]


Dr2d = np.kron(CD,np.eye(M))
D2r2d = np.kron(CD2,np.eye(M))

Dphi2D = np.kron(R,Dphi )
D2phi2D = np.kron(np.dot(R,R),D2phi )

def ApplyLap(B, IndI, IndJ, val):
	I1 = PNum * IndI
	J1 = PNum * IndJ
	I2 = PNum * (IndI+1)
	J2 = PNum * (IndJ+1)
	B[I1:I1 + PNum, J1:J1 + PNum] += val * (np.kron(CD2 + np.dot(R,CD),np.eye(M)) + np.kron(np.dot(R,R),D2phi)- np.kron(np.dot(R,R), np.eye(M) ) ) #RR
	B[I1:I1 + PNum, J2:J2 + PNum] += val * (-2.*np.kron(np.dot(R,R),Dphi ))#RP
	B[I2:I2 + PNum, J1:J1 + PNum] += val * (2.*np.kron(np.dot(R,R),Dphi ) )#PR
	B[I2:I2 + PNum, J2:J2 + PNum] += val * (np.kron(CD2 + CD,np.eye(M)) + np.kron(np.dot(R,R),D2phi) - np.kron(np.dot(R,R), np.eye(M) ) ) #PP

def ApplyNablaNabla(B, IndI, IndJ, val):
	I1 = PNum * IndI
	J1 = PNum * IndJ
	I2 = PNum * (IndI+1)
	J2 = PNum * (IndJ+1)
	B[I1:I1 + PNum, J1:J1 + PNum] += val * (np.kron(np.dot(R,CD),np.eye(M)) - np.kron(np.dot(R,R), np.eye(M) ) ) #RR
	B[I1:I1 + PNum, J2:J2 + PNum] += val * (np.dot(np.kron(R,Dphi), np.kron(CD,np.eye(M))) - np.kron(np.dot(R,R),Dphi)) #RP
	B[I2:I2 + PNum, J1:J1 + PNum] += val * (np.kron(np.dot(R,R),Dphi)+ np.dot(np.kron(R,Dphi), np.kron(CD,np.eye(M))) ) #PR
	B[I2:I2 + PNum, J2:J2 + PNum] += val * np.kron(np.dot(R,R),D2phi) #PP

	
def Dx(B, IndI, IndJ, D, val):
	II = PNum * IndI
	JJ = PNum * IndJ
	B[II:II + PNum, JJ:JJ + PNum] += D * val

def Factor(B, IndI, IndJ, val):
	II = PNum * IndI
	JJ = PNum * IndJ
	for i in range(PNum):
		B[II + i,JJ + i] += val

VarN = 5
u_r = 0
u_p = 1
v_r = 2
v_p = 3
pre = 4
urP = u_r*PNum
upP = u_p*PNum
vrP = v_r*PNum
vpP = v_p*PNum
pP = pre*PNum

A = np.zeros((VarN*PNum, VarN*PNum))

ApplyLap(A,u_r ,u_r, 0.1  )
#~ ApplyNablaNabla(A,u_r ,u_r, 0.1  )

ApplyLap(A,v_r ,v_r, 0.1  )
Dx(A, v_r, pre, Dr2d, -1.) #p->u_r
Dx(A, v_p, pre, Dphi2D, -1.) #p->u_phi

Dx(A, pre, u_r, Dr2d, 0.5) #U_r ->P
Dx(A, pre, u_r, np.kron(R,np.eye(M)),  0.5) #U_r ->P
Dx(A, pre, u_p, Dphi2D, 0.5) #U_phi->p
Dx(A, pre, v_r, Dr2d,  0.5) #U_r ->P
Dx(A, pre, v_r, np.kron(R,np.eye(M)),  0.5) #U_r ->P
Dx(A, pre, v_p, Dphi2D, 0.5) #U_phi->p
	

#RB, we're using dirichlet RB for now. The value 
A[urP:urP+M,:] = 0
A[:,urP:urP+M] = 0
A[upP:upP+M,:] = 0
A[vrP:vrP+M,:] = 0
A[:,vrP:vrP+M] = 0
A[vpP:vpP+M,:] = 0
A[:,vpP:vpP+M] = 0

for i in range(M): #make sure the values for the boundary points are identical to the rhs
	A[urP+i,urP+i] = 1.
	A[upP+i,upP+i] = 1.
	A[vrP+i,vrP+i] = 1.
	A[vpP+i,vpP+i] = 1.
A[pP,:] = 0.
A[pP,pP] = 1.
A[pP+1,:] = 0.
A[pP+1,pP+1] = 1.


#~ A = BuildMatrix(VarN*PNum, Dphi, D2phi2D, Drad2D, D2rad2D, Drad2Dr)
#~ AFD = BuildMatrix(VarN*PNum, Dphi, D2phi2D, Drad2D, D2rad2DFD, Drad2DrFD)
#~ AFD = BuildMatrix(VarN*PNum, dp1FD, D2phi2DFD, Drad2DFD, D2rad2DFD, Drad2DrFD)

print len(A)

b = np.zeros(VarN*PNum)
w = np.zeros(VarN*PNum)

M22 = spla.splu(A)
M_x = lambda x: M22.solve(x)
M2 = spla.LinearOperator((VarN*PNum, VarN*PNum), M_x)

#set force and/or RB
#~ b[(u_p+1)*PNum-5*M:(u_p+1)*PNum-3*M] = 5.
b[upP:upP+M] = 0.1 #"rotating cylinder"
#~ b[upP-2*M:upP-M] = -0.5 # force in r-direction at the inside 

start = time.clock()
xs,ys = np.mgrid[-CellR:CellR:400j, -CellR:CellR:400j]
for step in range(Endstep+1):
	
	w,info = spla.gmres(A,b, x0=w, M = M2 )
	
	if step % SaveStep == 0 or step == Endstep:
		run = step / SaveStep
		
		PlotFigure()
		print str(run)+' '+str(time.clock() - start)
	
print time.clock() - start
