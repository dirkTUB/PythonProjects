import os
import numpy as np
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla
from scipy import optimize

def FD1Per(m):
	DFD1 = np.zeros((m,m))
	
	h = 2*np.pi/m
	
	#periodic Boundary
	DFD1[0][1] = 1./2.
	DFD1[0][-1] = -1./2.
	DFD1[-1][-2] = -1./2.
	DFD1[-1][0] = 1./2.
	for i in range(1,len(DFD1)-1):
		DFD1[i][i+1] = 1./2.
		DFD1[i][i-1] = -1./2.
	
	return 1./h*DFD1
	
def FD2Per(m):
	DFD2 = np.zeros((m,m))
	
	h = 2*np.pi/m
	
	#periodic Boundary
	DFD2[0][0] = - 2.
	DFD2[0][1] = 1.
	DFD2[0][-1] = 1.
	DFD2[-1][-1] = - 2.
	DFD2[-1][-2] = 1.
	DFD2[-1][0] = 1.
	for i in range(1,len(DFD2)-1):
		DFD2[i][i] = - 2.
		DFD2[i][i+1] = 1.
		DFD2[i][i-1] = 1.
	
	return DFD2*1./h**2
	
def cheb(N, x0 = -1., x1 = 1.):
	D = np.empty((N+1, N+1))
	D[0,0] = (2. * N**2 + 1. )/6.
	D[N,N] = -(2. * N**2 + 1. )/6.
	
	x = np.empty(N1)
	for i in range(N1):
		x[i] = np.cos((i * np.pi)/N) 
	
	for i in range(1,N):
		D[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,N1):
		for j in range(0,N1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == N :
					ci = 2.
				if j == 0 or j == N :
					cj = 2.
				D[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	
	y = np.empty(N1)
	for i in range(N1):
		y[i] = (x[i]+1)/2 * x1
	
	return (2./(x1-x0) *D, y)
	
def BuildMatrix(D2):
	return np.eye(N1) - dt * Dcon * D2
	
	

def PlotFigure(x, C, run):
	f, ax3 = plt.subplots(2, 1)
	ax3[0].plot(x,C[:N1])
	ax3[1].plot(x,C[N1:])
	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','Heat_'+str(run)+'.png'),bbox_inches='tight')
	plt.close(f)

N = 50
LengthX = 100
dt = 0.001
Dcon = 200.
EndTime = 1000
SaveTime = 5

Prec = 3 #0 = no, 1 = FD, 2 = ILU, 3 = FD-ILU

Step = int(EndTime/dt)+1
SaveStep = int(SaveTime/dt)

N1 = N + 1
(CD, x) = cheb(N, 0, LengthX)
Dzero = np.zeros(N1)
DN = np.copy(CD)
DN[0] = Dzero
DN[-1] = Dzero
CD2 = np.dot(CD, CD)
D2N = np.dot(CD, DN)
Weight = linalg.pinv2(CD[:N,:N] )[0,:]

path = 'RDCheb'
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))


A = BuildMatrix(D2N)
Diff = dt * Dcon * D2N

start = time.clock()

print 'Cond :'+str( np.linalg.cond(A) )
#~ if Prec  == 1:
	#~ def applyPrec(v):   # Define action of preconditioner
		#~ return spla.spsolve(-csr_matrix(AFD),v)
	#~ M = spla.LinearOperator((len(A),len(A)),matvec=applyPrec, dtype=float)
#~ elif Prec == 2:
	#~ M22 = spla.spilu(A)
	#~ M_x = lambda x: M22.solve(x)
	#~ M = spla.LinearOperator((len(A), len(A)), M_x)
#~ elif Prec == 3:
	#~ M22 = spla.spilu(AFD)
	#~ M_x = lambda x: M22.solve(x)
	#~ M = spla.LinearOperator((len(A), len(A)), M_x)



def ReakFHN(vv):
	(U, W) = (vv[:N1], vv[N1:])
	
	uu = dt*( U*(U-a)*(1.-U)  - W + I )
	ww = dt*eps*(U - g*W)
	return np.hstack( (uu, ww))
def ReakBr(vv):
	(c, a) = (vv[:N1], vv[N1:])
	
	cc = 0.125*dt*(A + a*c**2 - B*c -c)
	aa = 0.125*dt*(-a*c**2 + B*c)
	return np.hstack( (cc, aa))

def ReakWP(vv):
	(c, a) = (vv[:N1], vv[N1:])
	tot =  (Ntot - np.dot(Weight,c[:-1]) )
	#~ tot =   min(tot, 50.)
	#~ print dt*kb *(c**2/(kka+ c**2)+ka) 
	cc = dt*(kb *(c**2/(kka+ c**2) +ka) *tot - kc * c)
	aa = 0*a

	return np.hstack( (cc, aa))

def applyChem(vv):
	(U, W) = (vv[:N1], vv[N1:])
	
	uu = U - np.dot(Diff,U)# - dt*( U*(U-a)*(1.-U)  - W + I )
	ww = W# - dt*eps*(U - g*W)
	return np.hstack( (uu, ww))

def applyChemMat(v):
	aa = np.zeros_like(v)
	for i in range(len(aa)):
		aa[:,i] = applyChem(v[:,i])
	return aa

I = 0.00
a = 0.002
g = 0.5
eps = 0.0024804

A = 1
B = 2.1

ka = 0.01
kb = 10.
kc = 10.
kka = 0.1

tol= 1e-8

ChemOP = spla.LinearOperator((2*N1,2*N1),matvec=applyChem,matmat=applyChemMat,dtype=float)
ChemOPMat = ChemOP.matmat(np.eye(2*N1))
ILUC = spla.splu(ChemOPMat)
def applyCILUPrec(v):        # Define action of preconditioner
	return ILUC.solve(v)
PC = spla.LinearOperator((2*N1,2*N1),matvec=applyCILUPrec,dtype=float)

RDA=2
if RDA == 0:
	Reak = ReakFHN
elif RDA == 1:
	Reak = ReakBr
elif RDA == 2:
	Reak = ReakWP

C = np.zeros(2*N1)

if RDA == 2:
	def RootFHN(c):
		return kb *(c**2/(kka+ c**2) +ka)  - kc * c
	
	print optimize.fixed_point(RootFHN, 0)
	#~ exit(1)
	
	C[:N1/2] = 0.4
	C[N1/2:N1] = 0.
	#~ C[i] = A + np.random.rand()*0.01
for i in range(N1):
	if RDA == 0:
		C[i] 	+= 0.5+0.5*np.tanh(3*(3000. - (200 - x[i])**2)/4000.)
		C[i+N1] += 0.05+0.05*np.tanh(3*(3000. - (120 - x[i])**2)/4000.)
	elif RDA == 1:
		C[i] = A + np.random.rand()*0.01
		C[i+N1] = B/A

Ntot = np.dot(Weight,C[:N1-1])*1.
PlotFigure(x, C, -1)

for t in range(Step):
	
	CNew, info = spla.gmres(ChemOP,C + Reak(C), M=PC, tol=1e-10)
		
	if t % SaveStep == 0 or t == Step-1:
		run = t / SaveStep
		print( str(run)+'_'+str(time.clock() - start)+'_'+str(np.dot(Weight,C[:N1-1])) )
		PlotFigure(x, CNew, run)
		
	CNew, C = C, CNew
