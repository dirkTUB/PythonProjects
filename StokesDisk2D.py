import os
import numpy as np
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla
import time

def Spec(n):#return spectral diff Matrix (periodic)
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	i = np.array( range(1,n) )
	column = np.hstack(( [0.], .5*(-1)**i/np.tan(i*h/2.) ))
	row = np.hstack(([0.], column[n-1:0:-1] ))
	D = linalg.toeplitz(column,row)
	return (D, x)
	
def FD1Per(m):
	DFD1 = np.zeros((m,m))
	
	h = 2*np.pi/m
	scale = 1./h
	
	#periodic Boundary
	DFD1[0][1] = scale/2.
	DFD1[0][-1] = -scale/2.
	DFD1[-1][-2] = -scale/2.
	DFD1[-1][0] = scale/2.
	for i in range(1,len(DFD1)-1):
		DFD1[i][i+1] = scale/2.
		DFD1[i][i-1] = -scale/2.
	
	return DFD1

def FD2Per(m):
	DFD2 = np.zeros((m,m))
	
	h = 2*np.pi/m
	
	#periodic Boundary
	DFD2[0][0] = - 2.
	DFD2[0][1] = 1.
	DFD2[0][-1] = 1.
	DFD2[-1][-1] = - 2.
	DFD2[-1][-2] = 1.
	DFD2[-1][0] = 1.
	for i in range(1,len(DFD2)-1):
		DFD2[i][i] = - 2.
		DFD2[i][i+1] = 1.
		DFD2[i][i-1] = 1.
	
	return DFD2*1./h**2

def cheb(n): #return cheb diff Matrix
	D = np.empty((n+1, n+1))
	D[0,0] = (2. * n**2 + 1. )/6.
	D[n,n] = -(2. * n**2 + 1. )/6.
	
	x = np.empty(n+1)
	for i in range(n+1):
		x[i] = np.cos((i * np.pi)/n) 
	
	for i in range(1,n):
		D[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,n+1):
		for j in range(0,n+1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == n :
					ci = 2.
				if j == 0 or j == n :
					cj = 2.
				D[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	return (D, x )
	
def FD1(x):
	DFD1 = np.zeros((len(x),len(x)))
	DFD1[0][0] = -1./abs(x[0]-x[1])
	DFD1[0][1] = 1./abs(x[0]-x[1])
	DFD1[-1][-1] = 1./abs(x[-1]-x[-2])
	DFD1[-1][-2] = -1./abs(x[-1]-x[-2])
	
	for i in range(1,len(DFD1)-1):
		x1 = abs(x[i-1]-x[i])
		x2 = abs(x[i+1]-x[i])
		DFD1[i][i] = (x2-x1)/(x1 * x2)
		DFD1[i][i+1] = x1/(x2*(x2 + x1))
		DFD1[i][i-1] = x2/(x1*(x1 + x2))
	return DFD1

def FD2(x):
	DFD2 = np.zeros((len(x),len(x)))
	
	DFD2[0][0] = -1./abs(x[0]-x[1])
	DFD2[0][1] = 1./abs(x[0]-x[1])
	DFD2[-1][-1] = -1./abs(x[-1]-x[-2])
	DFD2[-1][-2] = 1./abs(x[-1]-x[-2])
	
	for i in range(1,len(DFD2)-1):
		x1 = abs(x[i-1]-x[i])
		x2 = abs(x[i+1]-x[i])
		DFD2[i][i] = - 2./(x1 * x2)
		DFD2[i][i+1] = 2./(x2*(x2 + x1))
		DFD2[i][i-1] = 2./(x1*(x1 + x2))
	
	return -DFD2

def PlotFigure():
	brC = griddata(Knots, b[u_r*PNum:(u_r+1)*PNum], (xs, ys), method='nearest')
	bpC = griddata(Knots, b[u_p*PNum:(u_p+1)*PNum], (xs, ys), method='nearest')
	UrC = griddata(Knots, w[u_r*PNum:(u_r+1)*PNum], (xs, ys), method='nearest')
	UpC = griddata(Knots, w[u_p*PNum:(u_p+1)*PNum], (xs, ys), method='nearest')
	PresC = griddata(Knots, w[pre*PNum:(pre+1)*PNum], (xs, ys), method='nearest')
	rFDC = griddata(Knots, np.dot(D2rad2DFD,w[u_p*PNum:(u_p+1)*PNum]), (xs, ys), method='linear')
	rChC = griddata(Knots, np.dot(D2rad2D,w[u_p*PNum:(u_p+1)*PNum]), (xs, ys), method='linear')
	print np.dot(D2rad2DFD,w[u_p*PNum:(u_p+1)*PNum])[:M]
	print w[u_p*PNum:(u_p+1)*PNum][:2*M]
	fig, ax = plt.subplots(1, 5,figsize=(5*5,5))
	DatList= [UrC, UpC, PresC,rFDC, rChC]
	ax[0].set_title('u_r')
	ax[1].set_title('u_p')
	ax[2].set_title('pressure')
	for i in range(len(DatList)):
		im = ax[i].imshow(DatList[i].T, extent=[-CellR,CellR,-CellR,CellR])
		divider2 = make_axes_locatable(ax[i])
		cax2 = divider2.append_axes("right", size="8%", pad=0.05)
		cbar2 = plt.colorbar(im, cax=cax2)
	
	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','State{}.png'.format(run)))
	plt.close(fig)
	
	
	XvelR = w[u_r:(u_r+1)*PNum]*np.cos(Coord[:,1]) -w[u_p*PNum:(u_p+1)*PNum]*np.sin(Coord[:,1])
	YvelR = w[u_r:(u_r+1)*PNum]*np.sin(Coord[:,1]) + w[u_p*PNum:(u_p+1)*PNum]*np.cos(Coord[:,1])

	XvelP = -w[u_p*PNum:(u_p+1)*PNum]*np.sin(Coord[:,1])
	YvelP = w[u_p*PNum:(u_p+1)*PNum]*np.cos(Coord[:,1])
	
	fig, (ax1, ax2) = plt.subplots(1, 2,figsize=(2*5,5))
	ax1.set_title('rhs phi, vel in phi direction')
	im1 = ax1.matshow(bpC.T, extent=[-CellR,CellR,-CellR,CellR],cmap='cool')
	divider1 = make_axes_locatable(ax1)
	cax1 = divider1.append_axes("right", size="8%", pad=0.05)
	cbar1 = plt.colorbar(im1, cax=cax1)
	KnotMod = 1
	v = ax1.quiver([x[0] for x in Knots[::KnotMod]],[x[1] for x in Knots[::KnotMod]],XvelP[::KnotMod],YvelP[::KnotMod])
	
	ax2.set_title('rhs r, full velocity')
	im2 = ax2.matshow(brC.T, extent=[-CellR,CellR,-CellR,CellR],cmap='cool')
	divider2 = make_axes_locatable(ax2)
	cax2 = divider2.append_axes("right", size="8%", pad=0.05)
	cbar2 = plt.colorbar(im2, cax=cax2)
	v = ax2.quiver([x[0] for x in Knots[::KnotMod]],[x[1] for x in Knots[::KnotMod]],XvelR[::KnotMod],YvelR[::KnotMod])

	plt.tight_layout()
	plt.savefig(os.path.join(path,'Vel','Vel{}.png'.format(run)))
	plt.close(fig)
	
	
	print D2rad2DFD[-M]
	fig, ax = plt.subplots(1, 3,figsize=(3*5,5))
	
	plt.tight_layout()
	plt.savefig(os.path.join(path,'1D','1D{}.png'.format(run)))
	plt.close(fig)

CellR = 75.
Vis = 0.1
dt=0.01
Endstep=1000
SaveStep = Endstep/10

path = 'StokesDisk2D'
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))
if not os.path.exists(os.path.join(path,'Vel')): #create Folders for Output State
	os.makedirs(os.path.join(path,'Vel'))
if not os.path.exists(os.path.join(path,'1D')): #create Folders for Output State
	os.makedirs(os.path.join(path,'1D'))

#radius
N = 21 #odd
N2 = (N-1)/2
(CD, r) = cheb(N)

def ReSortFD(a):
	half = len(a)/2
	#resort columns
	b = np.empty_like(a)
	
	a1 = a[:,:half]
	a2 = np.fliplr(a[:,half:])
	print np.shape(a1)
	print np.shape(a2)
	print np.shape(b)
	for i in range(half):
		b[:,2*i] = a1[:,i]
		b[:,2*i+1] = a2[:,i]
	
	#resort rows
	b1 = b[:half]
	b2 = np.flipud(b[half:])
	for i in range(half):
		print i
		a[2*i] = b1[i]
		a[2*i+1] = b2[i]
		
		
	return a

#~ d1FD = ReSortFD(FD1(r))
d1FD = FD1(r*CellR)
d2FD = FD2(r*CellR)
r = r[:N2+1]*CellR # only positive r

CD = CD*1./CellR
CDN = np.copy(CD)
CDN[0] = 0
CDN[-1] = 0
CD2 = np.dot(CD, CD)
CD2N = np.dot(CD, CDN)
R = np.diag(1./r)

#theta
M = 10 #even
M2 = M/2
(Dphi,phi) = Spec(M)
D2phi = np.dot(Dphi,Dphi)

PNum= M*(N2+1) #Num of points (for each var)
Knots = np.empty((PNum,2))
Coord = np.empty((PNum,2))
for i in range(PNum):
	Knots[i][0] = r[i/(PNum/len(r))] * np.cos(phi[i%M])
	Knots[i][1] = r[i/(PNum/len(r))] * np.sin(phi[i%M])
	Coord[i][0] = r[i/(PNum/len(r))]
	Coord[i][1] = phi[i%M]

#construct Matrices for Dr
D1N = CD2N[:N2+1,:N2+1] #no flux
D2N = np.fliplr(CD2N[:N2+1,N2+1:])
E1N = CDN[:N2+1,:N2+1]
E2N = np.fliplr(CDN[:N2+1,N2+1:])

D1 = CD2[:N2+1,:N2+1] #no RB
D2 = np.fliplr(CD2[:N2+1,N2+1:])
E1 = CD[:N2+1,:N2+1]
E2 = np.fliplr(CD[:N2+1,N2+1:])

E1FD = d1FD[:N2+1,:N2+1]
D1FD = d2FD[:N2+1,:N2+1]
E2FD = np.fliplr(d1FD[:N2+1,N2+1:])
D2FD = np.fliplr(d2FD[:N2+1,N2+1:])

AA = np.vstack((np.hstack((np.zeros((M2,M2)),np.eye(M2))), np.hstack((np.eye(M2),np.zeros((M2,M2))))) )

dp1FD = FD1Per(M)
dp2FD = FD2Per(M)

Drad2D = (np.kron(E1,np.eye(M))+ np.kron(E2,AA))
Drad2Dr = (np.kron(np.dot(R,E1),np.eye(M))+ np.kron(np.dot(R,E2),AA))
D2rad2D = (np.kron(D1,np.eye(M))+ np.kron(D2,AA))

Drad2DFD = np.kron(E1FD,np.eye(M))+ np.kron(E2FD,AA)
Drad2DrFD = (np.kron(np.dot(R,E1FD),np.eye(M))+ np.kron(np.dot(R,E2FD),AA))
D2rad2DFD = (np.kron(D1FD,np.eye(M))+ np.kron(D2FD,AA))

Dphi2D = np.kron(R,Dphi )
D2phi2D = np.kron(np.dot(R,R),D2phi )
Dphi2DFD = np.kron(R,dp1FD )
D2phi2DFD = np.kron(np.dot(R,R),dp2FD )
#~ Dphi2D = np.kron(R,Dphi )

def ApplyLap(B, IndI, IndJ, val, d1p, d2p,dr1r, dr2):
	I1 = PNum * IndI
	J1 = PNum * IndJ
	I2 = PNum * (IndI+1)
	J2 = PNum * (IndJ+1)
	B[I1:I1 + PNum, J1:J1 + PNum] += val * (dr1r+ dr2 + d2p - np.kron(np.dot(R,R), np.eye(M) ) ) #RR
	B[I1:I1 + PNum, J2:J2 + PNum] += val * (-2.*np.kron(np.dot(R,R),d1p ))#RP
	B[I2:I2 + PNum, J1:J1 + PNum] += val * (2.*np.kron(np.dot(R,R),d1p ) )#PR
	B[I2:I2 + PNum, J2:J2 + PNum] += val * (dr1r + dr2 + d2p - np.kron(np.dot(R,R), np.eye(M) ) ) #PP

def ApplyDphi2D(B, IndI, IndJ, val):
	II = PNum * IndI
	JJ = PNum * IndJ
	B[II:II + PNum, JJ:JJ + PNum] += (np.kron(R,Dphi )) * val
	
def Dx(B, IndI, IndJ, D, val):
	II = PNum * IndI
	JJ = PNum * IndJ
	B[II:II + PNum, JJ:JJ + PNum] += D * val

VarN = 5
u_r = 0
u_p = 1
v_r = 2
v_p = 3
pre = 4
urP = u_r*PNum
upP = u_p*PNum
vrP = v_r*PNum
vpP = v_p*PNum
pP = pre*PNum

def BuildMatrix(nn, dp1, dp2, dr1, dr2, dr1r):
	dp1r = np.kron(R,dp1 )
	
	a = np.zeros((nn, nn))
	ApplyLap(a,u_r ,u_r, Vis,dp1, dp2,dr1r, dr2  )
	Dx(a, u_r, pre, dr1, -1) #p->u_r
	Dx(a, u_p, pre, dp1r, -1.) #p->u_phi

	ApplyLap(a,v_r ,v_r, Vis, dp1, dp2,dr1r, dr2  )
	Dx(a, v_r, pre, dr1, -1) #p->u_r
	Dx(a, v_p, pre, dp1r, -1.) #p->u_phi

	Dx(a, pre, u_r, dr1, 0.5) #U_r ->P
	Dx(a, pre, u_r, np.kron(R,np.eye(M)),  0.5) #U_r ->P
	Dx(a, pre, u_p, dp1r, 0.5) #U_phi->p
	Dx(a, pre, v_r, dr1,  0.5) #U_r ->P
	Dx(a, pre, v_r, np.kron(R,np.eye(M)),  0.5) #U_r ->P
	Dx(a, pre, v_p, dp1r, 0.5) #U_phi->p
	
	#RB, we're using dirichlet RB for now. The value 
	a[urP:urP+M,:] = 0
	a[:,urP:urP+M] = 0
	a[upP:upP+M,:] = 0
	a[vrP:vrP+M,:] = 0
	a[:,vrP:vrP+M] = 0
	a[vpP:vpP+M,:] = 0
	a[:,vpP:vpP+M] = 0
	
	for i in range(M): #make sure the values for the boundary points are identical to the rhs
		a[urP+i,urP+i] = 1.
		a[upP+i,upP+i] = 1.
		a[vrP+i,vrP+i] = 1.
		a[vpP+i,vpP+i] = 1.
	a[pP,:] = 0.
	a[pP,pP] = 1.
	a[pP+1,:] = 0.
	a[pP+1,pP+1] = 1.
	
	return a


A = BuildMatrix(VarN*PNum, Dphi, D2phi2D, Drad2D, D2rad2D, Drad2Dr)
AFD = BuildMatrix(VarN*PNum, Dphi, D2phi2D, Drad2D, D2rad2DFD, Drad2DrFD)
#~ AFD = BuildMatrix(VarN*PNum, dp1FD, D2phi2DFD, Drad2DFD, D2rad2DFD, Drad2DrFD)

print len(A)

b = np.zeros(VarN*PNum)
w = np.zeros(VarN*PNum)

M22 = spla.splu(A)
M_x = lambda x: M22.solve(x)
M2 = spla.LinearOperator((VarN*PNum, VarN*PNum), M_x)

#set force and/or RB
#~ b[(u_p+1)*PNum-5*M:(u_p+1)*PNum-3*M] = 5.
b[upP:upP+M] = 0.1 #"rotating cylinder"
b[upP-2*M:upP-M] = -0.5 # force in r-direction at the inside 

start = time.clock()
xs,ys = np.mgrid[-CellR:CellR:400j, -CellR:CellR:400j]
for step in range(Endstep+1):
	
	w,info = spla.gmres(A,b, x0=w, M = M2 )
	
	if step % SaveStep == 0 or step == Endstep:
		run = step / SaveStep
		
		PlotFigure()
		print str(run)+' '+str(time.clock() - start)
	
print time.clock() - start
