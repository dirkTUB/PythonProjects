import os
import numpy as np
import scipy as sp
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla
from scipy.interpolate import griddata
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pyamg as pa
from scipy.fftpack import idct, dct
from chebfft import Dchebfft, D2chebfft, Dfft, D2fft

def Spec(n):
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	i = np.array( range(1,n) )
	column = np.hstack(( [0.], .5*(-1)**i/np.tan(i*h/2.) ))
	row = np.hstack(([0.], column[n-1:0:-1] ))
	D = linalg.toeplitz(column,row)
	return (D, x)

def cheb(n): #return cheb diff Matrix
	DTemp = np.empty((n+1, n+1))
	DTemp[0,0] = (2. * n**2 + 1. )/6.
	DTemp[n,n] = -(2. * n**2 + 1. )/6.
	
	x = np.empty(n+1)
	for i in range(n+1):
		x[i] = np.cos((i * np.pi)/n) 
	
	for i in range(1,n):
		DTemp[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,n+1):
		for j in range(0,n+1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == n :
					ci = 2.
				if j == 0 or j == n :
					cj = 2.
				DTemp[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	return (DTemp, x)

def PlotFigure(x, C, b, run):
	fig, ax = plt.subplots(1, 2, figsize = (2*5,5))
	
	ax[0].plot(C[:PNum])
	ax[0].plot(b[:PNum])
	ax[1].plot(C[PNum:])
	
	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','State{0:04d}.png'.format(run)),bbox_inches='tight')
	plt.close(plt.gcf())

CellR = 75
InitialR = 2
dt = 0.01
Dcon = 1
EndTime = 0.1
SaveTime = 0.01
Bound = 2 #0 = Dir, 1 = no Flux, 2: per

path = 'StokesFFT1D'
Steps = int(EndTime/dt)+1
SaveStep = int(SaveTime/dt)
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))
if not os.path.exists(os.path.join(path,'FFT')): #create Folders for Output State
	os.makedirs(os.path.join(path,'FFT'))

#phi
M = 20 #even
if Bound == 0:
	PNum = M+1
	(Dphi,phi) = cheb(M)
	D2phi = np.dot(Dphi,Dphi)
if Bound == 2:
	PNum = M
	(Dphi,phi) = Spec(M)
	
	
VarPnum = 2* PNum

w = np.zeros(2*PNum)
b = np.zeros(2*PNum)

#~ b[:PNum] = np.sin(phi*np.pi)*0.00000005
#~ b[0] = 0.0005

def counter(xk):
    print("Krylov residual:\t%10.3e" % xk)

dk1 = np.empty(M)
dk2 = np.empty(M)
for i in range(M/2):
	dk1[i] = i
	dk2[i] = i
for i in range(1,M/2):
	dk1[M-i] = -i
	dk2[M-i] = -i
if M %2 ==0:
	dk1[M/2] = 0
k2= -dk2**2
k1 = dk1
def D1fft(v):
	#~ U = np.imag()
	#~ print 'v'
	#~ print v
	#~ print 'U'
	#~ print U
	#~ print 'w'
	w = np.imag(np.fft.ifft(k1*np.fft.fft(v)))
	print w
	return w
def D2fft(v):
	U = np.real(np.fft.fft(v))
	return np.real(np.fft.ifft(k2*U))


eta=0.1
def CD1(v):
	
	if Bound == 0:
		cd1 = Dchebfft(v)
	elif Bound == 2:
		
		cd1 = D1fft(v)
		
	return cd1
def CD2(v):
	if Bound == 0:
		cd2 = D2chebfft(v)
	elif Bound == 2:
		cd2 = D2fft(v)
	return cd2

def applyDFBC(v):
	(U, P) = (v[:PNum], v[PNum:])
	UU = eta *CD2(U) - CD1(U)
	PP = CD1(P)
	
	if Bound == 0:
		UU[0] = 0.
		UU[-1] = 0.
		PP[0] = 0.
	if Bound == 2:
		UU[0] = 1.
		UU[1] = 1.
		PP[0] = 0.
		PP[1] = 0.
	return np.hstack((UU, PP))
def applyMat(v):
	a = np.zeros_like(v)
	

	for i in range(len(a)):
		a[:,i] = applyDFBC(v[:,i])
	
	if Bound == 0:
		a[0,:] = 0
		a[0,0] = 1.
		a[PNum-1,:] = 0
		a[PNum-1,PNum-1] = 1.
		a[PNum,:] = 0.
		a[PNum,PNum] = 1.
	if Bound == 2:
		a[0,:] = 0
		a[0,0] = 1.
		a[1,:] = 0
		a[1,0] = 1.
		a[PNum,:] = 0.
		a[PNum,PNum] = 1.
		a[PNum+1,:] = 0.
		a[PNum+1,PNum+1] = 1.
	return a
FFTOP = spla.LinearOperator((VarPnum,VarPnum),matvec=applyDFBC,matmat=applyMat,dtype=float)
FFTOPMat = FFTOP.matmat(np.eye(VarPnum))
print('Matrix rank:',np.linalg.matrix_rank(FFTOPMat))
print('Matrix cond:',np.linalg.cond(FFTOPMat))

ILU = spla.splu(FFTOPMat)
def applyILUPrec(v):        # Define action of preconditioner
	return ILU.solve(v)
PILU = spla.LinearOperator((VarPnum,VarPnum),matvec=applyILUPrec,dtype=float)

PlotFigure(phi, w, b, -1)

for step in range(Steps):
	#~ b[:PNum] = np.sin(phi*2*np.pi*step + step/10*2*np.pi)
	wNew, info   = spla.gmres(FFTOP	,b, M=PILU, callback=counter, tol =1e-10)
	
	if step % SaveStep == 0:
		run = step / SaveStep
		PlotFigure(phi, wNew, b, run)
		print str(run)
		#~ print wNew[:PNum]
		#~ print wNew[PNum:]

	wNew, w = w, wNew
