import os
import numpy as np
import scipy as sp
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import time
from scipy.sparse import csr_matrix
import scipy.sparse.linalg as spla
from scipy.interpolate import griddata
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pyamg as pa
from FFTDeriv import Dchebfft, D2chebfft, D1fft, D2fft

def Spec(n):
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	return x

def cheb(n): #return cheb diff Matrix
	DTemp = np.empty((n+1, n+1))
	DTemp[0,0] = (2. * n**2 + 1. )/6.
	DTemp[n,n] = -(2. * n**2 + 1. )/6.
	
	x = np.empty(n+1)
	for i in range(n+1):
		x[i] = np.cos((i * np.pi)/n) 
	
	for i in range(1,n):
		DTemp[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,n+1):
		for j in range(0,n+1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == n :
					ci = 2.
				if j == 0 or j == n :
					cj = 2.
				DTemp[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	return (DTemp, x)

def PlotFigure():
	UrC = griddata(Knots, w[UrP:Ur1P], (xs, ys), method='nearest')
	UpC = griddata(Knots, w[UpP:Up1P], (xs, ys), method='nearest')
	urC = griddata(Knots, w[urP:ur1P], (xs, ys), method='nearest')
	upC = griddata(Knots, w[upP:up1P], (xs, ys), method='nearest')
	VrC = griddata(Knots, w[VrP:Vr1P], (xs, ys), method='nearest')
	VpC = griddata(Knots, w[VpP:Vp1P], (xs, ys), method='nearest')
	PresC = griddata(Knots, w[prP:pr1P], (xs, ys), method='nearest')
	BrC = griddata(Knots, b[u_r*PNum:(u_r+1)*PNum], (xs, ys), method='nearest')
	BpC = griddata(Knots, b[u_p*PNum:(u_p+1)*PNum], (xs, ys), method='nearest')
	BPresC = griddata(Knots, b[pre*PNum:(pre+1)*PNum], (xs, ys), method='nearest')
	
	fig, ax = plt.subplots(2, 4,figsize=(4*5,2*5))
	DatList= [[UrC,urC ,VrC, PresC],[UpC,upC, VpC, PresC]]
	ax[0][0].set_title('U_r')
	ax[0][1].set_title('u_r')
	ax[0][2].set_title('V_r')
	ax[0][3].set_title('pressure')
	ax[1][0].set_title('U_p')
	ax[1][1].set_title('u_p')
	ax[1][2].set_title('v_p')
	ax[1][3].set_title('pressure')
	for i in range(len(DatList)):
		for j in range(len(DatList[i])):
			im = ax[i][j].imshow(DatList[i][j].T, extent=[-CellR,CellR,-CellR,CellR])
			divider2 = make_axes_locatable(ax[i][j])
			cax2 = divider2.append_axes("right", size="8%", pad=0.05)
			cbar2 = plt.colorbar(im, cax=cax2)
	
	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','State{0:04d}.png'.format(run)),bbox_inches='tight')
	plt.close(plt.gcf())
	
	fig, ax = plt.subplots(1, 3,figsize=(3*5,5))
	DatList= [BrC, BpC, BPresC]
	ax[0].set_title('b_r')
	ax[1].set_title('b_p')
	ax[2].set_title('b_pres')
	for i in range(len(DatList)):
		im = ax[i].imshow(DatList[i].T, extent=[-CellR,CellR,-CellR,CellR])
		divider2 = make_axes_locatable(ax[i])
		cax2 = divider2.append_axes("right", size="8%", pad=0.05)
		cbar2 = plt.colorbar(im, cax=cax2)
	
	plt.tight_layout()
	plt.savefig(os.path.join(path,'B','State{0:04d}.png'.format(run)),bbox_inches='tight')
	plt.close(plt.gcf())

def counter(xk):
    print("Krylov residual:\t%10.3e" % xk)


def SpecCoef(m):
	dk1 = np.empty(m)
	dk2 = np.empty(m)
	for i in range(m/2):
		dk1[i] = i
	for i in range(m/2+1):
		dk2[i] = i
	for i in range(m/2+1,m):
		dk1[i] = i-m
		dk2[i] = i-m
	if m %2 ==0:
		dk1[m/2] = 0
		
	dk1r = np.empty((Nr,m))
	dk2r = np.empty((Nr,m))
	
	for i in range(Nr):
		dk1r[i] = dk1/r[i]
		dk2r[i] = -(dk2/r[i])**2
	
	return (dk1r, dk2r)

def Rad(v,r): # this function modifies the value given to it, use copy
	vp0 = np.reshape(v,(Nr,M))
	for i in range(Nr):
		vp0[i] *=r[i]
	return vp0.flatten()

def ApplyVecLap(v):
	(Vr, Vp) = (v[:PNum], v[PNum:])
	VR = d2r(Vr) + Rad(d2p(Vr),rd2) + Rad(dr(Vr), rd1) - 2.*Rad(dp(Vp),rd2) - Rad(np.copy(Vr),rd2)
	VP = d2r(Vp) + Rad(d2p(Vp),rd2) + Rad(dr(Vp), rd1) + 2.*Rad(dp(Vr),rd2) - Rad(np.copy(Vp),rd2)
	
	return np.hstack((VR,VP))
	
def dr(Vr):
	dr = np.empty_like(Vr)
	for i in range(M2):
		rr = np.hstack( (Vr[i::M], Vr[i+M2::M][::-1] ))
		back = 1./CellR*Dchebfft(rr)
		dr[i::M] = back[:Nr]
		dr[i+M2::M] = -back[Nr:][::-1] #e_r points into the other direction for negative r 
	return dr
	
def d2r(Vr):
	dr = np.empty_like(Vr)
	for i in range(M2):
		rr = np.hstack( (Vr[i::M], Vr[i+M2::M][::-1] ))
		back = 1./(CellR**2)*D2chebfft(rr)
		dr[i::M] = back[:Nr]
		dr[i+M2::M] = back[Nr:][::-1]
	return dr
	
def dp(Vp):
	vp0 = np.reshape(Vp,(Nr,M))
	dp = np.empty((Nr,M))
	for i in range(Nr): #iterate trough the circles with different radii and thus different wavenumbers
		dp[i,:] = D1fft(vp0[i,:], k1[i])
	return dp.flatten()
	
def d2p(Vp):
	vp0 = np.reshape(Vp,(Nr,M))
	dp = np.empty((Nr,M))
	for i in range(Nr):
		dp[i,:] = D2fft(vp0[i,:], k2[i])
	return dp.flatten()
	
def Grad(P):
	return np.hstack( (dr(P),Rad(dp(P), rd1)) )
	
def Div(v):
	(Vr, Vp) = (v[:PNum], v[PNum:])
	return dr(Vr)+Rad(np.copy(Vr),rd1) + Rad(dp(Vp),rd1)

def applyDFBC(v):
	(U, u, V,  P) = (v[UrP:Up1P], v[urP:up1P], v[VrP:Vp1P], v[prP:pr1P])
	
	UU = U-u/dt
	uu = eta*ApplyVecLap(u) + eta*ApplyVecLap(U)- Grad(P)
	VV = eta*ApplyVecLap(V) - Grad(P)
	PP = 0.5*Div(U) + 0.5*Div(V) + 0.0000001*P
	
	if Bound == 'dir':
		uu[:M] = b[urP:urP+M]
		uu[PNum:PNum+M] = b[upP:upP+M]
		VV[:M] = b[VrP:VrP+M]
		VV[PNum:PNum+M] = b[VpP:VpP+M]
		
		uu[PNum+10*M:PNum+11*M] = b[upP+10*M:upP+11*M]
		#~ UU[PNum+M] = 0.0
		PP[-1] = 0.
		PP[-2] = 0.
	
	return np.hstack((UU, uu, VV, PP))
def applyMat(v):
	a = np.zeros_like(v)
	

	for i in range(len(a)):
		a[:,i] = applyDFBC(v[:,i])
	
	if Bound == 'dir':
		a[urP:urP+M,:] = 0
		a[:,urP:urP+M] = 0
		a[upP:upP+M,:] = 0
		a[:,upP:upP+M] = 0
		a[VrP:VrP+M,:] = 0
		a[:,VrP:VrP+M] = 0
		a[VpP:VpP+M,:] = 0
		a[:,VpP:VpP+M] = 0
		a[upP+10*M:upP+11*M,:] = 0

		for i in range(M):
			a[urP+i,urP+i] = 1.
			a[upP+i,upP+i] = 1.
			a[VrP+i,VrP+i] = 1.
			a[VpP+i,VpP+i] = 1.
			a[upP+i+10*M,upP+i+10*M] = 1.
		
		a[-1,:] = 0.
		a[-1,-1] = 1.
		a[-2,:] = 0.
		a[-2,-2] = 1.
	return a
	
CellR = 1
InitialR = 2
N = 31 #odd
N2 = (N-1)/2
Nr = N2+1
M = 12 #even
M2 = M/2
dt = 0.01
eta=0.1
Dcon = 1
EndTime = 0.01
SaveTime = 0.01
Bound = 'dir'

path = 'StokesFFT2D'
Steps = int(EndTime/dt)+1
SaveStep = int(SaveTime/dt)
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))
if not os.path.exists(os.path.join(path,'B')): #create Folders for Output State
	os.makedirs(os.path.join(path,'B'))
xs,ys = np.mgrid[-CellR:CellR:1000j, -CellR:CellR:1000j]

VarN = 7
PNum= M*Nr #Num of points (for each var)
VarPnum = VarN* PNum
U_r = 0
U_p = 1
u_r = 2
u_p = 3
V_r = 4
V_p = 5
pre = 6

UrP = U_r*PNum
Ur1P = (U_r+1)*PNum
UpP = U_p*PNum
Up1P = (U_p+1)*PNum
urP = u_r*PNum
ur1P = (u_r+1)*PNum
upP = u_p*PNum
up1P = (u_p+1)*PNum
VrP = V_r*PNum
Vr1P = (V_r+1)*PNum
VpP = V_p*PNum
Vp1P = (V_p+1)*PNum
prP = pre*PNum
pr1P = (pre+1)*PNum

(CD, r) = cheb(N)
CD *= 1./CellR
r = r[:N2+1]*CellR 
rd1 = 1./r
rd2 = 1./(r**2)

phi = Spec(M)
(k1, k2) = SpecCoef(M)

w = np.zeros(VarPnum)
b = np.zeros(VarPnum)

Knots = np.empty((PNum,2))
Coord = np.empty((PNum,2))
for i in range(PNum):
	Knots[i][0] = r[i/(PNum/len(r))] * np.cos(phi[i%M])
	Knots[i][1] = r[i/(PNum/len(r))] * np.sin(phi[i%M])
	Coord[i][0] = r[i/(PNum/len(r))]
	Coord[i][1] = phi[i%M]

#~ b[-1] = 1.
#~ b[-M] = 5.
#~ b[PNum-5*M] = 0.5
#~ b[PNum-M/4] = 50.
#~ b[PNum-5*M- M/4] = -0.5
#~ b[:M] = -0.
b[upP+10*M:upP+11*M] = -2.
#~ b[3*PNum+15*M:3*PNum+16*M] = -2.
b[VpP+15*M:VpP+16*M] = -2.

#~ TEST = np.zeros(2*PNum)
#~ for i in range(PNum):
	#~ TEST[i] = 0.5+0.5*np.tanh(3*(0.5-Coord[i][0])/0.4)
	#~ TEST[i+PNum] = np.sin(Coord[i][1])
#~ tr = TEST[:PNum]
#~ tp = TEST[PNum:PNum+M]

#~ back1 = np.hstack( (dr(tr)[::M], dr(tr)[M2::M][::-1] ))
#~ back2 = np.hstack( (d2r(tr)[::M], d2r(tr)[M2::M][::-1] ))

#~ rr = np.hstack( (tr[::M], tr[M2::M][::-1] ))

#~ fig, ax = plt.subplots(1, 3,figsize=(3*5,1*5))
#~ ax[0].plot(tp)
#~ ax[0].plot(D1fft(tp, k1[0]))
#~ ax[0].plot(D2fft(tp,k2[0]))
#~ ax[1].plot(rr)
#~ ax[1].plot(back1)
#~ ax[1].plot(back2)
#~ ax[2].plot(rr)
#~ ax[2].plot(np.dot(CD,rr))
#~ ax[2].plot(np.dot(np.dot(CD,CD),rr))

#~ plt.tight_layout()
#~ plt.savefig(os.path.join(path,'Dif.png'),bbox_inches='tight')
#~ plt.close(plt.gcf())

#~ exit(1)
#~ T1 = ApplyVecLap(TEST)
#~ T0r = griddata(Knots, TEST[:PNum], (xs, ys), method='linear')
#~ T0p = griddata(Knots, TEST[u_p*PNum:(u_p+1)*PNum], (xs, ys), method='linear')
#~ T1r = griddata(Knots, T1[u_r*PNum:(u_r+1)*PNum], (xs, ys), method='linear')
#~ T1p = griddata(Knots, T1[u_p*PNum:(u_p+1)*PNum], (xs, ys), method='linear')

#~ fig, ax = plt.subplots(2, 2,figsize=(2*5,2*5))
#~ DatList= [T0r, T0p]
#~ DatList2= [T1r, T1p]
#~ ax[0][0].set_title('Tr')
#~ ax[0][1].set_title('Tp')
#~ for i in range(len(DatList)):
	#~ im = ax[0][i].imshow(DatList[i].T, extent=[-CellR,CellR,-CellR,CellR])
	#~ divider2 = make_axes_locatable(ax[0][i])
	#~ cax2 = divider2.append_axes("right", size="8%", pad=0.05)
	#~ cbar2 = plt.colorbar(im, cax=cax2)
#~ for i in range(len(DatList2)):
	#~ im = ax[1][i].imshow(DatList2[i].T, extent=[-CellR,CellR,-CellR,CellR])
	#~ divider2 = make_axes_locatable(ax[1][i])
	#~ cax2 = divider2.append_axes("right", size="8%", pad=0.05)
	#~ cbar2 = plt.colorbar(im, cax=cax2)

#~ plt.tight_layout()
#~ plt.savefig(os.path.join(path,'Test.png'),bbox_inches='tight')
#~ plt.close(plt.gcf())

FFTOP = spla.LinearOperator((VarPnum,VarPnum),matvec=applyDFBC,matmat=applyMat,dtype=float)
FFTOPMat = FFTOP.matmat(np.eye(VarPnum))
print len(FFTOPMat)
print('Matrix rank:',np.linalg.matrix_rank(FFTOPMat))
print('Matrix cond:',np.linalg.cond(FFTOPMat))
#~ exit(1)
ILU = spla.splu(FFTOPMat)
def applyILUPrec(v):        # Define action of preconditioner
	return ILU.solve(v)
PILU = spla.LinearOperator((VarPnum,VarPnum),matvec=applyILUPrec,dtype=float)

for step in range(Steps):
	b[UrP:Up1P] = w[urP:up1P]/dt
	wNew, info   = spla.gmres(FFTOP	,b, M=PILU, callback=counter, tol=1e-9)
	
	wNew, w = w, wNew
	if step % SaveStep == 0:
		run = step / SaveStep
		PlotFigure()
		print str(run)
		

	
