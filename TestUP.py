import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pickle
from scipy import linalg
from scipy.fftpack import idct, dct

def FD1(x):
	N1 = len(x)
	DFD1 = np.zeros((N1,N1))
	DFD1[0][0] = 1./abs(x[0]-x[1])**2
	DFD1[0][1] = -1./abs(x[0]-x[1])**2
	DFD1[-1][-1] = -1./abs(x[-1]-x[-2])**2
	DFD1[-1][-2] = 1./abs(x[-1]-x[-2])**2
	
	for i in range(1,len(DFD1)-1):
		x1 = abs(x[i-1]-x[i])
		x2 = abs(x[i+1]-x[i])
		DFD1[i][i] = (x2-x1)/(x1 * x2)
		DFD1[i][i+1] = x1/(x2*(x2 + x1))
		DFD1[i][i-1] = x2/(x1*(x1 + x2))
	return DFD1

def FD2(x):
	N1 = len(x)
	DFD2 = np.zeros((N1,N1))
	
	DFD2[0][0] = 1./abs(x[0]-x[1])**2
	DFD2[0][1] = -1./abs(x[0]-x[1])**2
	DFD2[-1][-1] = -1./abs(x[-1]-x[-2])**2
	DFD2[-1][-2] = 1./abs(x[-1]-x[-2])**2
	
	for i in range(1,len(DFD2)-1):
		x1 = abs(x[i-1]-x[i])
		x2 = abs(x[i+1]-x[i])
		DFD2[i][i] = - 2./(x1 * x2)
		DFD2[i][i+1] = 2./(x2*(x2 + x1))
		DFD2[i][i-1] = 2./(x1*(x1 + x2))
	
	return DFD2

def DchebDCT(v, m):
	n=len(v)
	fac = 1/np.sqrt(2*(n-1))
	U = dct(v, type=1)*fac #type 1 does not support normalization yet
	
	for i in range(m): # mth derivative
		U1 = np.copy(U)
		U[-1] = 0
		U[-2] = 0
		for i in range(len(U)-2, 0, -1):
			U[i-1] = 2*i*U1[i] + U[i+1]
	w = idct(U, type=1)*fac
	return w

def dr(Vr):
	dr = np.empty_like(Vr)
	for i in range(M2):
		rr = np.hstack( (Vr[i::M], -Vr[i+M2::M][::-1] ))
		back = 1./CellR*DchebDCT(rr,1)
		dr[i::M] = back[:Nr]
		dr[i+M2::M] = back[Nr:][::-1] #e_r points into the other direction for negative r 
	return dr

def d2r_FD(Vr):
	dr = np.empty_like(Vr)
	for i in range(M2):
		rr = np.hstack( (Vr[i::M], -Vr[i+M2::M][::-1] ))
		back = np.dot(CD2_FD, rr)
		dr[i::M] = back[:Nr]
		dr[i+M2::M] = -back[Nr:][::-1]
	return dr
	
def d2r(Vr):
	#~ dr = np.empty_like(Vr)
	#~ for i in range(M):
		#~ dr[i::M] = 1./(CellR**2)*DchebDCT(Vr[i::M],2)
	return dr(dr(Vr))
	
def d2rb(Vr):
	dr = np.empty_like(Vr)
	for i in range(M2):
		rr = np.hstack( (Vr[i::M], -Vr[i+M2::M][::-1] ))
		back = 1./(CellR**2)*DchebDCT(rr,2)
		dr[i::M] = back[:Nr]
		dr[i+M2::M] = back[Nr:][::-1] #e_r points into the other direction for negative r 
	return dr

def chebPoints(n):
	x = np.empty(n+1)
	for i in range(n+1):
		x[i] = np.cos((i * np.pi)/n) 
	return x

def cheb(x): #return cheb diff Matrix
	n=len(x)-1
	D = np.empty((n+1, n+1))
	D[0,0] = (2. * n**2 + 1. )/6.
	D[n,n] = -(2. * n**2 + 1. )/6.
	

	
	for i in range(1,n):
		D[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,n+1):
		for j in range(0,n+1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == n :
					ci = 2.
				if j == 0 or j == n :
					cj = 2.
				D[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	return D

def Spec(n):#return spectral diff Matrix (periodic)
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	i = np.array( range(1,n) )
	column = np.hstack(( [0.], .5*(-1)**i/np.tan(i*h/2.) ))
	row = np.hstack(([0.], column[n-1:0:-1] ))
	D = linalg.toeplitz(column,row)
	return (D, x)

pkl_file = open('W0017.pkl', 'rb')
#~ pkl_file = open('W0033.pkl', 'rb')
up = pickle.load(pkl_file)
PNum=len(up)

N = 41
N2 = (N-1)/2
Nr = N2+1
M=20
CellR=75

r = chebPoints(N)
CD = cheb(r)
CD1_FD = FD1(r*CellR)
CD2_FD = FD2(r*CellR)
r = r[:N2+1]*CellR
CD = CD*1./CellR
CD2 = np.dot(CD, CD)

R = np.diag(1./r)
rd1 = 1./r
rd2 = 1./(r**2)

#theta

M2 = M/2
(Dphi,phi) = Spec(M)
D2phi = np.dot(Dphi,Dphi)

Knots = np.empty((PNum,2))
Coord = np.empty((PNum,2))
for i in range(PNum):
	Knots[i][0] = r[i/(PNum/len(r))] * np.cos(phi[i%M])
	Knots[i][1] = r[i/(PNum/len(r))] * np.sin(phi[i%M])
	Coord[i][0] = r[i/(PNum/len(r))]
	Coord[i][1] = phi[i%M]

D1 = CD2[:N2+1,:N2+1] 
D2 = np.fliplr(CD2[:N2+1,N2+1:])
E1 = CD[:N2+1,:N2+1]
E2 = np.fliplr(CD[:N2+1,N2+1:])
E1_FD = CD1_FD[:N2+1,:N2+1]
E2_FD = np.fliplr(CD1_FD[:N2+1,N2+1:])

AA = np.vstack((np.hstack((np.zeros((M2,M2)),np.eye(M2))), np.hstack((np.eye(M2),np.zeros((M2,M2))))) )

Drad2D = np.kron(E1,np.eye(M))
Drad2D_FD = np.kron(E1_FD,np.eye(M))
D2rad2D = np.kron(D1,np.eye(M))
Dphi2D = np.kron(R,Dphi )
E12 = np.copy(E1)
for i in range(Nr):
	E12[i,-1] += sum(E2[i])



drup = np.dot(Drad2D_FD,up)
drupDCT = dr(up)
d2rup = np.dot(D2rad2D,up)
d2rupDCTa = d2r(up)
d2rupDCTb = d2rb(up)
d2rupFD = d2r_FD(up)

drup2 = np.dot(np.kron(E12,np.eye(M)),up)
fig, ax = plt.subplots(3, 2, figsize = (3*5,2*5))
ax[0][0].set_title('$u_p$')
ax[0][0].tricontourf(Knots[:,0],Knots[:,1],up, 500)
ax[0][0].set_aspect('equal')

ax[0][1].set_title('$\partial_r u_p$ (Full r-axis, Matrix)')
ax[0][1].tricontourf(Knots[:,0],Knots[:,1],drup, 500)
ax[0][1].set_aspect('equal')

ax[1][0].set_title('$\partial_r u_p$ (Full r-axis, DCT)')
ax[1][0].tricontourf(Knots[:,0],Knots[:,1],drupDCT, 500)
ax[1][0].set_aspect('equal')

ax[1][1].set_title('$\partial_r u_p$ (Half r-axis)')
ax[1][1].tricontourf(Knots[:,0],Knots[:,1],drup2, 500)
ax[1][1].set_aspect('equal')

i=2
ax[2][0].plot(range(0,Nr),up[i::M],label='u_P, r+')
ax[2][0].plot(range(Nr,2*Nr),-up[i+M2::M][::-1],label='u_P, r-')
ax[2][0].legend(loc='best')
#~ ax[2][1].plot(range(0,Nr),drup[i::M],label='$\partial_r u_P$, Full r')
ax[2][1].plot(range(0,Nr),drupDCT[i::M],label='$\partial_r u_P$, Full r+ DCT')
#~ ax[2][1].plot(range(0,Nr),drup2[i::M],label='$\partial_r u_P$, Only r+')
#~ ax[2][1].plot(range(Nr,2*Nr),drup[i+M2::M][::-1])
ax[2][1].plot(range(Nr,2*Nr),drupDCT[i+M2::M][::-1])
#~ ax[2][1].plot(range(Nr,2*Nr),drup2[i+M2::M][::-1])
ax[2][1].legend(loc='best')
plt.tight_layout()
plt.savefig('Dr1.png')
plt.close(fig)

fig, ax = plt.subplots(3, 2, figsize = (3*5,2*5))
ax[0][0].set_title('$u_p$')
ax[0][0].tricontourf(Knots[:,0],Knots[:,1],up, 500)
ax[0][0].set_aspect('equal')

ax[0][1].set_title('$\partial^2_r u_p$ (Full r-axis)')
ax[0][1].tricontourf(Knots[M:-M,0],Knots[M:-M,1],d2rup[M:-M], 500)
ax[0][1].set_aspect('equal')

ax[1][0].tricontourf(Knots[M:-M,0],Knots[M:-M,1],d2rupDCTb[M:-M], 500)
ax[1][0].set_aspect('equal')

#~ ax[1][1].tricontourf(Knots[M:-M,0],Knots[M:-M,1],d2rupDCTb[M:-M], 500)
ax[1][1].tricontourf(Knots[M:-M,0],Knots[M:-M,1],d2rupFD[M:-M], 500)
ax[1][1].set_aspect('equal')

i=2

d2rFull = np.hstack(  (d2rup[M:-M][i::M], d2rup[M:-M][i::M][::-1]))
d2rDCTa = np.hstack(  (d2rupDCTa[M:-M][i::M], d2rupDCTa[M:-M][i::M][::-1]))
d2rDCTb = np.hstack(  (d2rupDCTb[M:-M][i::M], d2rupDCTb[M:-M][i::M][::-1]))
d2rFD = np.hstack(  (d2rupFD[M:-M][i::M], d2rupFD[M:-M][i::M][::-1]))
ax[2][0].plot(range(0,Nr),up[i::M],label='u_P, r+')
ax[2][0].plot(range(Nr,2*Nr),-up[i+M2::M][::-1],label='u_P, r-')
ax[2][0].legend(loc='best')
ax[2][1].plot(d2rFull,label='$\partial^2_r u_P$, Full r')
#~ ax[2][1].plot(d2rDCTa,label='$\partial^2_r u_P$, DCT half R')
#~ ax[2][1].plot(d2rDCTb,label='$\partial^2_r u_P$, DCT Full R')
ax[2][1].plot(d2rFD,label='$\partial_r u_P$, FD')
#~ ax[2][1].plot(range(0,Nr),drup2[i::M],label='$\partial_r u_P$, Only r+')
#~ ax[2][1].plot(range(Nr,2*Nr),drup[i+M2::M][::-1])
#~ ax[2][1].plot(range(Nr,2*Nr),drupDCT[i+M2::M][::-1])
ax[2][1].legend(loc='best')


plt.tight_layout()
plt.savefig('Dr2.png')
plt.close(fig)

