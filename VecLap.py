import os
import numpy as np
from scipy import linalg
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.sparse import csr_matrix
import pyamg as pa

def Spec(n):#return spectral diff Matrix (periodic)
	h = 2*np.pi/n
	x = h*np.arange(1,n+1)
	i = np.array( range(1,n) )
	column = np.hstack(( [0.], .5*(-1)**i/np.tan(i*h/2.) ))
	row = np.hstack(([0.], column[n-1:0:-1] ))
	D = linalg.toeplitz(column,row)
	return (D, x)

def cheb(n): #return cheb diff Matrix
	D = np.empty((n+1, n+1))
	D[0,0] = (2. * n**2 + 1. )/6.
	D[n,n] = -(2. * n**2 + 1. )/6.
	
	x = np.empty(n+1)
	for i in range(n+1):
		x[i] = np.cos((i * np.pi)/n) 
	
	for i in range(1,n):
		D[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,n+1):
		for j in range(0,n+1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == n :
					ci = 2.
				if j == 0 or j == n :
					cj = 2.
				D[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	return (D, x )

def PlotFigure():
	DC = np.dot(Lap,C)
	xs,ys = np.mgrid[-CellR:CellR:500j, -CellR:CellR:500j]
	CR = griddata(Knots, C[:PNum], (xs, ys), method='linear')
	CP = griddata(Knots, C[PNum:], (xs, ys), method='linear')
	DCR = griddata(Knots, DC[:PNum], (xs, ys), method='linear')
	DCP = griddata(Knots, DC[PNum:], (xs, ys), method='linear')

	fig, ax = plt.subplots(2, 2)
	im0 = ax[0][0].imshow(CR, extent=[-CellR,CellR,-CellR,CellR])
	divider0 = make_axes_locatable(ax[0][0])
	cax0 = divider0.append_axes("right", size="8%", pad=0.05)
	cbar0 = plt.colorbar(im0, cax=cax0)

	im1 = ax[0][1].imshow(CP, extent=[-CellR,CellR,-CellR,CellR])
	divider1 = make_axes_locatable(ax[0][1])
	cax1 = divider1.append_axes("right", size="8%", pad=0.05)
	cbar1 = plt.colorbar(im1, cax=cax1)

	im3 = ax[1][0].imshow(DCR, extent=[-CellR,CellR,-CellR,CellR])
	divider3 = make_axes_locatable(ax[1][0])
	cax3 = divider3.append_axes("right", size="8%", pad=0.05)
	cbar3 = plt.colorbar(im3, cax=cax3)

	im4 = ax[1][1].imshow(DCP, extent=[-CellR,CellR,-CellR,CellR])
	divider4 = make_axes_locatable(ax[1][1])
	cax4 = divider4.append_axes("right", size="8%", pad=0.05)
	cbar4 = plt.colorbar(im4, cax=cax4)
	plt.tight_layout()
	plt.savefig(os.path.join(path,'State','Lap{}.png'.format(run)))
	plt.close(fig)

def MultiGrid(ATemp):
	B = np.ones((ATemp.shape[0],1))
	smooth=('jacobi', {'omega': 4.0/3.0,'degree':2})
	SA_build_args={'max_levels':10, 'max_coarse':5}
	SA_solve_args={'cycle':'V', 'maxiter':10, 'tol':1e-9}
	strength=[('evolution')]
	presmoother=('block_gauss_seidel', {'sweep': 'symmetric'})
	postsmoother=('block_gauss_seidel', {'sweep': 'symmetric'})
	improve_candidates =[('block_gauss_seidel', {'sweep': 'symmetric', 'iterations': 4}), None]
	MG = pa.smoothed_aggregation_solver(csr_matrix(ATemp),B,  BH = B.copy(), keep=True, smooth=smooth, strength=strength, improve_candidates=improve_candidates, presmoother=presmoother, postsmoother=postsmoother, **SA_build_args)
	return (MG, SA_solve_args)

CellR = 75.
path = 'VecLap'
if not os.path.exists(os.path.join(path,'State')): #create Folders for Output State
	os.makedirs(os.path.join(path,'State'))

#radius
N = 51 #odd
N2 = (N-1)/2
(CD, r) = cheb(N)
r = r[:N2+1]*CellR # only positive r
CD = CD*1./CellR
CDN = np.copy(CD)
CDN[0] = 0
CDN[-1] = 0
CD2 = np.dot(CD, CD)
CD2N = np.dot(CD, CDN)
R = np.diag(1./r)

#theta
M = 20 #even
M2 = M/2
(Dphi,phi) = Spec(M)
D2phi = np.dot(Dphi,Dphi)

PNum= M*(N2+1) #Num of points (for each var)
Knots = np.empty((PNum,2))
Coord = np.empty((PNum,2))
for i in range(PNum):
	Knots[i][0] = r[i/(PNum/len(r))] * np.cos(phi[i%M])
	Knots[i][1] = r[i/(PNum/len(r))] * np.sin(phi[i%M])
	Coord[i][0] = r[i/(PNum/len(r))]
	Coord[i][1] = phi[i%M]

#construct Matrices
Rfull = np.kron(R,np.eye(M))
AA = np.vstack((np.hstack((np.zeros((M2,M2)),np.eye(M2))), np.hstack((np.eye(M2),np.zeros((M2,M2))))) )

D1D = np.copy( CD2[:N2+1,:N2+1] )
D1D[0] = 0
D1D[:,0] = 0
E1D = np.copy( CD[:N2+1,:N2+1] )
E1D[0] = 0
E1D[:,0] = 0

D2D = np.copy( np.fliplr(CD2[:N2+1,N2+1:]) )
D2D[0] = 0
D2D[:,0] = 0

E2D = np.copy( np.fliplr(CD[:N2+1,N2+1:]) )
E2D[0] = 0
E2D[:,0] = 0

D1N = CD2N[:N2+1,:N2+1]
D2N = np.fliplr(CD2N[:N2+1,N2+1:])
E1N = CDN[:N2+1,:N2+1]
E2N = np.fliplr(CDN[:N2+1,N2+1:])

D1 = CD2[:N2+1,:N2+1]
D2 = np.fliplr(CD2[:N2+1,N2+1:])
E1 = CD[:N2+1,:N2+1]
E2 = np.fliplr(CD[:N2+1,N2+1:])
	


LapRR = np.kron(D1D+np.dot(R,E1D),np.eye(M)) + np.kron(D2D+np.dot(R,E2D),AA) + np.kron(np.dot(R,R),D2phi)- np.kron(np.dot(R,R), np.eye(M) )
LapRP = -2.*np.kron(np.dot(R,R),Dphi )
LapPP = np.kron(D1D+np.dot(R,E1D),np.eye(M)) + np.kron(D2D+np.dot(R,E2D),AA) + np.kron(np.dot(R,R),D2phi) - np.kron(np.dot(R,R), np.eye(M) )
LapPR = 2.*np.kron(np.dot(R,R),Dphi )
Lap = np.vstack( (np.hstack( (LapRR, LapRP) ), np.hstack( (LapPR, LapPP) ) ))

C = np.zeros(PNum*2)
for i in range(PNum):
	C[i] = 0. + 0.001*np.cos(2*np.pi * Coord[i][0]/CellR)
	C[i+PNum] = 1. #cost flow in phi direction

Dcon = 10
dt=0.01
A = np.eye(2*PNum) - dt * Dcon * Lap

(MG, SolveArg) = MultiGrid(A)

Endstep=1000
SaveStep = Endstep/10
#Diffusion
for step in range(Endstep+1):
	CNew = MG.solve(C,x0=C, **SolveArg)
	
	if step % SaveStep == 0 or step == Endstep:
		run = step / SaveStep
		
		PlotFigure()
		print run
	CNew, C = C, CNew
	
