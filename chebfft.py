# CHEBFFT  Chebyshev differentiation via FFT. Simple, not optimal.  
#          If v is complex, delete "real" commands.
from numpy import *
from numpy.fft import fft,ifft

def Dchebfft(v):
	N = len(v)-1; 
	if N==0: return 0
	x = cos(arange(0,N+1)*pi/N)
	ii = arange(0,N); iir = arange(1-N,0); iii = array(ii,dtype=int)
	#v = v[:]
	V = hstack((v,v[N-1:0:-1])) # transform x -> theta 
	U = real(fft(V))
	W = real(ifft(1j*hstack((ii,[0.],iir))*U))
	w = zeros(N+1)
	w[1:N] = -W[1:N]/sqrt(1-x[1:N]**2)    # transform theta -> x     
	w[0] = sum(iii**2*U[iii])/N + .5*N*U[N]     
	w[N] = sum((-1)**(iii+1)*ii**2*U[iii])/N + .5*(-1)**(N+1)*N*U[N]
	return w

def D2chebfft(v):
	N = len(v)-1; 
	if N==0: return 0
	x = cos(arange(0,N+1)*pi/N)
	ii = arange(0,N); iir = arange(1-N,0); iii = array(ii,dtype=int)
	#v = v[:]
	V  = hstack((v,v[N-1:0:-1])) # transform x -> theta 
	U  = real(fft(V))
	W1 = real(ifft(1j*hstack((ii,[0.],iir))*U))
	W2 = real(ifft(-(hstack((ii,[N],iir)))**2*U))
	w  = zeros(N+1)
	w[1:N] = W2[1:N]/(1-x[1:N]**2) - x[1:N]*W1[1:N]/((1-x[1:N]**2)**(3.0/2.0))    # transform theta -> x     
	w[0] = (sum(iii**2*(iii**2-1.0)*U[iii]) + 0.5*N**2*(N**2-1.0)*U[N])/(3.0*N)
	w[N] = (sum((-1)**(iii)*iii**2*(iii**2-1.0)*U[iii]) + 0.5*(-1)**N*N**2*(N**2-1.0)*U[N])/(3.0*N)
	return w

def Dfft(v):
	M = len(v)-1; 
	ii = arange(0,M); iir = arange(1-M,0); iii = array(ii,dtype=int)
	U = real(fft(v))
	W = real(ifft(1j*hstack((ii,[0.],iir))*U))
	return w
	
def D2fft(v, rrr, dd):
	M = len(v); 
	k = empty(M)
	
	
	for i in range(M/2):
		k[i] = rrr**2 *i * dd
	for i in range(1,M/2):
		k[M-i] = -rrr**2 *i * dd
	
	U = real(fft(v))
	W = real(ifft(k*U))
	return W
